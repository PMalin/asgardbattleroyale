
addPacketListener(Packets.Message, function(packet)
{
    switch(packet.readUInt8())
    {
        case MesssagePackets.Receive:
            local mid = packet.readInt16();
            local message = packet.readString();

            if(Messages.rawin(mid))
                Messages[mid].addMessage(message, MessageType.Received);
            else
                MessageStorage(mid).addMessage(message, MessageType.Received);
        break;
    }
})