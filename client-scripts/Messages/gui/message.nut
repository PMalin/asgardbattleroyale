function MessageGUI::Message(messageObj)
{
    local data = messageObj.data;
    local value = messageObj.message;

    return StringLib.textWrap("("+data+") "+value, 34);
}