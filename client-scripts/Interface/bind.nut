
local bindedKeys = [];

function bindKey(_keyId, _func, _playerActiveGUI = null)
{
    bindedKeys.append({
        keyId = _keyId,
        func = _func,
        playerActiveGUI = _playerActiveGUI,
    })
}

local function keyHandler(key)
{
    if (chatInputIsOpen())
		return

	if (isConsoleOpen())
		return

    foreach(bind in bindedKeys)
    {
        if(bind.keyId == key)
        {
            if(bind.playerActiveGUI == null && Player.GUI == false)
            {
                bind.func();
                return;
            }

            if(bind.playerActiveGUI == Player.GUI)
            {
                bind.func();
                return;
            }
        }
    }
}

addEventHandler("onKey", keyHandler)