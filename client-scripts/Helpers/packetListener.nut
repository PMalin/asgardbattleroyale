
local listeners = [];

function addPacketListener(id, _func) {
    listeners.append({ id = id, func = _func});
}

function packetListener(packet) {
    local id = packet.readUInt8();

    foreach(listen in listeners)
        if(listen.id == id)
            listen.func(packet);
}