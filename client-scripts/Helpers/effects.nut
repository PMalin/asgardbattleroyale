
class _EffectRegister
{
    objects = null;
    check = null;

    constructor()
    {
        objects = [];
        check = getTickCount();
    }

    /**
    Throw Stone
    */
    function Throw(botId, fromPosition, toPosition) {
        if(toPosition == null)
            return;

        toPosition.y = toPosition.y - 100;

        objects.append({
            typeId = 0,
            botId = botId,
            vob = Vob("MIN_MOB_STONE_V1_20.3DS"),
            toPosition = toPosition,
            hitted = false,
            timer = 0,
            distance = getDistance2d(fromPosition.x, fromPosition.z, toPosition.x, toPosition.z),
            end = false,
        })

        local _angle = getVectorAngle(fromPosition.x,fromPosition.z,toPosition.x,toPosition.z);

        objects[objects.len()-1].vob.addToWorld()
        objects[objects.len()-1].vob.setPosition(fromPosition.x, fromPosition.y + 100, fromPosition.z);
        objects[objects.len()-1].vob.setRotation(0, _angle, 0);        
    }

    function ThrowForward(object) {
        local vob = object.vob, currentPosition = vob.getPosition(), toPosition = object.toPosition, rotation = vob.getRotation(), baseDistance = object.distance, timer = object.timer;

        local distance = getDistance2d(currentPosition.x, currentPosition.z, toPosition.x, toPosition.z);
        if(object.timer < 6) {
            currentPosition.y = currentPosition.y + (1 * 64/8);
            object.timer = object.timer + 1;
        }else
            currentPosition.y = currentPosition.y - (1 * 64/4);

        currentPosition.x = currentPosition.x + (sin(rotation.y * 3.14 / 180.0) * 64);
        currentPosition.z = currentPosition.z + (cos(rotation.y * 3.14 / 180.0) * 64);		
        vob.setPosition(currentPosition.x, currentPosition.y, currentPosition.z);

        if(object.hitted == false)
        {
            local pos = getPlayerPosition(heroId)
            if(getDistance3d(currentPosition.x, currentPosition.y, currentPosition.z, pos.x, pos.y, pos.z) < 100)
            {
                object.hitted = true;
                object.end = true;
                createBoss.hitPlayer(object.botId);
                return;
            }
        }

        if(currentPosition.y <= toPosition.y - 300) {
            object.end = true;
        }       
    }

    function Mountain(botId, fromPosition, toPosition) {
        objects.append({
            typeId = 1,
            botId = botId,
            vob = Vob("MIN_MOB_STONE_V1_20.3DS"),
            toPosition = toPosition,
            hitted = false,
            end = false
        })
        objects[objects.len()-1].vob.addToWorld();
        objects[objects.len()-1].vob.setPosition(toPosition.x, toPosition.y + 2000, toPosition.z);        
    }

    function MountainForward(object) {
        local vob = object.vob, currentPosition = vob.getPosition(), targetPosition = object.toPosition;

        currentPosition.y = currentPosition.y - (10 * 64/8);
        vob.setPosition(currentPosition.x, currentPosition.y, currentPosition.z);

        if(object.hitted == false)
        {
            local pos = getPlayerPosition(heroId)
            if(getDistance3d(currentPosition.x, currentPosition.y, currentPosition.z, pos.x, pos.y, pos.z) < 100)
            {
                object.hitted = true;
                createBoss.hitPlayer(object.botId);
                return;
            }
        }

        if(currentPosition.y <= targetPosition.y - 300) {
            object.end = true;
        }     
    }

    function RoundXardas(botId, position, elementId) {
        addEffect(elementId, "spellFX_INCOVATION_VIOLET");

        local pos = getPlayerPosition(heroId);
        if(getDistance3d(position.x, position.y, position.z, pos.x, pos.y, pos.z) < 600)
            createBoss.hitPlayer(botId);
    }

    function RoundSzaman(botId, position, elementId) {
        addEffect(elementId, "spellFX_INCOVATION_RED");

        local pos = getPlayerPosition(heroId);
        if(getDistance3d(position.x, position.y, position.z, pos.x, pos.y, pos.z) < 600)
            createBoss.hitPlayer(botId);
    }

    function RoundVatras(botId, position, elementId) {
        addEffect(elementId, "spellFX_INCOVATION_BLUE");

        local pos = getPlayerPosition(heroId);
        if(getDistance3d(position.x, position.y, position.z, pos.x, pos.y, pos.z) < 800)
            createBoss.hitPlayer(botId);
    }

    function onRender() {
        if(check > getTickCount())
            return

        foreach(ind, obj in objects)
            if(obj.end == true)
                objects.remove(ind);

        foreach(ind, obj in objects)
        {
            switch(obj.typeId)
            {
                case 0:
                    ThrowForward(obj);
                break;
                case 1:
                    MountainForward(obj);
                break;
            }
        }

        check = getTickCount() + 50;
    }
}

EffectRegister <- _EffectRegister();