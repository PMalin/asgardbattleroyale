
function playAniIfOther(pid, anim) {
    if(getPlayerAni(pid) != anim)
        playAni(pid, anim);
}


local lastAnim;

addEventHandler("onRender", function() {
    if(lastAnim != getPlayerAni(heroId))
    {
        lastAnim = getPlayerAni(heroId);
        callEvent("onPlayerChangeAnimation", lastAnim);
    }
});