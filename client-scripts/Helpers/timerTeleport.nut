local check = getTickCount();
local currentPositionOfTimerTeleport = [0, 500, 0], currentPositionOfTimerTeleportCount = 60;
local vobOfCurrentPosition = Vob("EVT_SPAWN_01.3DS");
local active = false;

class TimerTeleport
{
    function Active() 
    {
        active = true;
    }
    function Deactive() 
    {
        active = false;   
        vobOfCurrentPosition.setPosition(0,-3000,0); 
    }
    function IsActive() 
    {
        return active;    
    }
    function GetPosition() 
    {
        return currentPositionOfTimerTeleport;   
    }
    function onRender() 
    {
        if(active == false)
            return;

        if(check > getTickCount())
            return;

        local pos = getPlayerPosition(heroId);
        if(getDistance3d(pos.x, pos.y, pos.z, currentPositionOfTimerTeleport[0], currentPositionOfTimerTeleport[1], currentPositionOfTimerTeleport[2]) < 300)
            setPlayerPosition(heroId, 5781.85,12585.3,-2887.26);

        vobOfCurrentPosition.setPosition(currentPositionOfTimerTeleport[0],currentPositionOfTimerTeleport[1]+100,currentPositionOfTimerTeleport[2]);
        
        currentPositionOfTimerTeleportCount = currentPositionOfTimerTeleportCount + 1;
        if(currentPositionOfTimerTeleportCount >= 60)
        {
            local positionOfTeleport = [
                [-3853.75, -138.984, -799.609],
                [15748.6, 417.188, -2765.39],
                [18017.7, -771.094, 8857.89],
                [4337.03, -1100.39, 7189.06],
                [-8463.67, -1236.17, 5412.5],
                [-15692.7, -509.297, 13133],
                [-23052.8, -372.422, 16907.3],
                [-27360.7, -656.641, 21082.9],
                [-30696.3, 258.594, 17144],
                [-30718.4, 868.359, 11395.5],
                [-30767.2, -369.844, 6954.22],
                [-37932.7, 307.266, 388.125],
                [-28311.3, 810.391, -4808.83],
                [-19547.6, 1154.61, -13966.3],
                [-12432.8, 1296.56, -15459.1],
                [-16389.5, 2337.11, -19933],
                [-9954.22, 2881.02, -22254.1],
                [-5215.16, 2403.52, -20615.2],
                [-152.5, 2808.75, -17813.4],
                [437.266, 552.578, -15898],
            ];
            vobOfCurrentPosition = Vob("EVT_SPAWN_01.3DS");
            
            local randomPositionId = rand() % positionOfTeleport.len();
            currentPositionOfTimerTeleport = positionOfTeleport[randomPositionId];
            currentPositionOfTimerTeleportCount = 0;
        }

        check = getTickCount() + 1000;
    }
}

