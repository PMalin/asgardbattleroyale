local list = [];

for(local i = 0; i <= getMaxSlots(); i++)
    list.append(null);

function getAllPlayers() {
    return list;
}

function getPlayerSlot(pid) {
    return list[pid];
}

function addPlayerToSlot(pid, name) {
    list[pid] = name;

    callEvent("onPlayerGetSlot", pid)
}

function removePlayerFromSlot(pid) {
    list[pid] = null;

    callEvent("onPlayerLostSlot", pid)
}