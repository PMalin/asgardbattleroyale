
addPacketListener(Packets.Structure, function(packet)
{
    switch(packet.readUInt8())
    {
        case StructurePackets.Add:
            local id = packet.readInt16();
            local owner = packet.readInt16();
            local health = packet.readInt16();
            local maxHealth = packet.readInt16();
            local btype = packet.readInt8();
            local posx = packet.readFloat();
            local posy = packet.readFloat();
            local posz = packet.readFloat();
            local angle = packet.readInt16();

            local struct = Structure(id, owner, btype, health, maxHealth, posx, posy, posz, angle);
        break;
        case StructurePackets.Remove:
            local id = packet.readInt16();
            local structure = getStructure(id);

            if(structure)
                structure.remove();
            else
                print("FATAL ERROR: STRUCTURE REMOVING ID NOT EXIST "+id)
        break;
        case StructurePackets.Hit:
            local id = packet.readInt16();
            local health = packet.readInt16();
            local structure = getStructure(id);

            if(structure)
                structure.setHealth(health);
            else
                print("FATAL ERROR: STRUCTURE ID NOT EXIST "+id)
        break;
    }
})
