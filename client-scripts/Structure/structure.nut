local All = [];

class Structure
{
    id = -1;
    bType = -1;

    health = 0;
    maxHealth = 0;

    owner = -1;
    position = null;
    element = null;

    draw = null;

    constructor(id, owner, btype, health, maxHealth, posx, posy, posz, angle)
    {
        this.id = id;
        this.bType = btype;

        position = {x = posx,y = posy,z = posz,angle = angle};

        this.owner = owner;
        this.health = health;
        this.maxHealth = maxHealth;

        draw = Draw3d(posx,posy + 100,posz);
        draw.visible = true;
        draw.distance = 1500;
        draw.top();
        draw.setColor(0, 190, 160);
        draw.insertText(health+"/"+maxHealth);

        switch(btype)
        {
            case StructureType.Locket:
                element = Vob("CHESTBIG_NW_RICH_LOCKED.ASC");
            break;
            case StructureType.Box:
                element = Vob("NW_HARBOUR_CRATE_01.3DS");
            break;
            case StructureType.Bush:
                element = Vob("NW_NATURE_BUSH_25P.3DS");
            break;
            case StructureType.WoodenWall:
                element = Vob("EVT_NC_MAINGATE01.3DS");
            break;
            case StructureType.Wall:
                element = Vob("NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            break;
            case StructureType.WallHorizontal:
                element = Vob("NW_DRAGONISLE_INVISIBLEORCWALL_01.3DS");
            break;
            case StructureType.Stairs:
                element = Vob("NC_STAIRS_V01.3DS");
            break;
            case StructureType.Krate:
                element = Vob("EVT_GATE_SMALL_01.3DS");
            break;
            case StructureType.Bonus:
                element = Vob("SKULL.3DS");
            break;
        }

        element.addToWorld()
        element.setPosition(posx, posy, posz);
        element.setRotation(0, angle, 0);

        if(bType == StructureType.WallHorizontal)
            element.setRotation(90, angle, 0);

        element.cdDynamic = true;
        element.cdStatic = true;

        if(btype == StructureType.Krate && owner != -1)
        {
            if(PlayerClan[owner].clan_id != -1)
            {
                if(PlayerClan[heroId].clan_id == PlayerClan[owner].clan_id)
                {
                    element.cdDynamic = false;
                    element.cdStatic = false;
                }
            }
            if(heroId == owner) {
                element.cdDynamic = false;
                element.cdStatic = false;
            }
        }

        if(btype == StructureType.Locket)
            element.floor();

        All.append(this);
    }

    function setHealth(value) {
        health = value;

        draw.setLineText(0, health+"/"+maxHealth);
    }

    function remove() {
        foreach(indsx, strct in All)
        {
            if(strct.id == id)
                All.remove(indsx);
        }
    }

    static function getClosest(pid)
    {
        local pos = getPlayerPosition(heroId);
        local selectedStruct = null;
        local distanceToStruct = 250;
        foreach(struct in getStructures())
        {
            local dist = getDistance3d(pos.x, pos.y, pos.z, struct.position.x, struct.position.y, struct.position.z);
            if(dist < distanceToStruct)
            {
                selectedStruct = struct;
                distanceToStruct = dist;
            }
        }
        return selectedStruct;
    }
}

function getStructure(id) {
    foreach(indsx, strct in All)
    {
        if(strct.id == id)
            return strct;
    }

    return null;
}

getStructures <- @() All;

local animAcceptedAsHit = [
    "S_FISTATTACK",
    "S_FISTATTACKMOVE",
    "T_1HATTACKL",
    "T_1HATTACKR",
    "S_1HATTACK",
    "T_2HATTACKL",
    "T_2HATTACKR",
    "S_2HATTACK",
]

addEventHandler("onPlayerChangeAnimation", function(anim)
{
    if(animAcceptedAsHit.find(anim) != null)
    {
        local struct = Structure.getClosest(heroId);
        if(struct)
        {
            local packet = Packet();
            packet.writeUInt8(Packets.Structure);
            packet.writeUInt8(StructurePackets.Hit);
            packet.writeInt16(struct.id);
            packet.send(RELIABLE_ORDERED);
        }
    }
});


function StructureEndGame()
{
    All.clear();
}