
addPacketListener(Packets.Draws, function(packet)
{
    switch(packet.readUInt8())
    {
        case DrawPackets.Create:
            add3dDraw(packet.readString(), packet.readString(), packet.readFloat(), packet.readFloat(), packet.readFloat(), packet.readInt16(),packet.readInt16(),packet.readInt16())
        break;
        case DrawPackets.Destroy:
            remove3dDraw(packet.readString());
        break;
    }
})