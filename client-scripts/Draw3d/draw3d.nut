local All = {};

function add3dDraw(id, text, x, y, z, r = 255, g = 255, b = 255) {
    local draw3d = Draw3d(x, y, z)
    draw3d.visible = true
    draw3d.distance = 1000
    draw3d.top()
    draw3d.setColor(r, g, b)
    draw3d.insertText(text)

    All[id] <- draw3d(text, x, y, z, r, g, b);
}

function remove3dDraw(id) {
    All.rawdelete(id);
}