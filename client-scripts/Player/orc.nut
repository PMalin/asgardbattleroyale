local vob = Vob("ORC_HEADWARRIOR.MMS");
vob.setRotation(0, 0, 90);

local orcPlayer = -1;

function setOrcPlayer(orcId) {
    orcPlayer = orcId;

    vob = Vob("ORC_HEADWARRIOR.MMS");
    vob.addToWorld()
    vob.setRotation(0, 0, 90);

    print("New player orc "+orcPlayer);
}

function getOrcPlayer() {
    return orcPlayer;
}

addEventHandler("onRender", function () {
    local pos = getPlayerPosition(orcPlayer);
    if(pos == null)
        return;

    vob.setRotation(getPlayerAngle(orcPlayer), 0, 90);
    vob.setPosition(pos.x, pos.y + 130, pos.z);
})