
addPacketListener(Packets.Player, function(packet)
{
    switch(packet.readUInt8())
    {
        case PlayerPackets.UseSkill:
            onPlayerUseSkill(packet.readInt16(), packet.readUInt8())
        break;
        case PlayerPackets.ResetSkill:
            Player.Skill.timeouts[packet.readUInt8()] = packet.readInt16();
        break;
        case PlayerPackets.Hearts:
            Player.hearts = packet.readInt16();
            onPlayerGetHeartsGUI();
        break;
        case PlayerPackets.BuildingPoints:
            Player.buildingPoints = packet.readInt16();
            onPlayerGetBuildingPointsGUI();
        break;
        case PlayerPackets.Orc:
            setOrcPlayer(packet.readInt16());
        break;
        case PlayerPackets.AdditionalPackets:
            local kills = packet.readInt16();
            local deaths = packet.readInt16();
            local winGames = packet.readInt16();
            local avatarId = packet.readInt16();

            Player.kills = kills;
            Player.deaths = deaths;
            Player.winGames = winGames;
            Player.avatarId = avatarId;

            onPlayerGetAdditionalData();
        break;
        case PlayerPackets.Effect:
            addEffect(packet.readInt16(), packet.readString());
        break;
        case PlayerPackets.Observer:
            Observer.show();
            Player.isObserver = true;
        break;
        case PlayerPackets.PlayVideo:
            playVideo("ASGARDBATTLEROYALE.BIK");
        break;
        case PlayerPackets.Clan:
            local pid = packet.readInt16();
            local clanId = packet.readInt16();

            setPlayerClan(pid, clanId);
        break;
        case PlayerPackets.ClearIventory:
            clearIventory();
        break;
        case PlayerPackets.UpdateSkill:
            setPlayerSkillTimeouts(packet.readInt16(), packet.readInt16(), packet.readInt16(), packet.readInt16(), packet.readInt16());
        break;
    }
})