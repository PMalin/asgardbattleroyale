local pidtohelpers = {};

class createHelper extends Bot
{
    helperUserId = -1;

    constructor(id, streamer, name, posx, posy, posz, angle, helperUserId, armor, melee, ranged, headTxt, headModel, bodyTxt, bodyModel)
    {
        base.constructor(id, streamer, name);

        setBotPosition(id, posx, posy, posz);    
        setBotAngle(id, angle);

        this.armor = armor;
        this.melee = melee;
        this.ranged = ranged;
        
        this.headTxt = headTxt;
        this.headModel = headModel;
        this.bodyTxt = bodyTxt;
        this.bodyModel = bodyModel;

        pidtohelpers[element] <- helperUserId;

        this.helperUserId = helperUserId;
    }

    function spawn() {
        spawned = true;

        setPlayerInstance(element, "PC_HERO");

        setPlayerMaxHealth(element,1000);
        setPlayerHealth(element, health);
        setPlayerStrength(element, 100);

        setPlayerAngle(element, angle);
        setPlayerPosition(element, position.x, position.y, position.z);

        setPlayerSkillWeapon(element, 0, 50);

        if(animation != "STOP")
            playAni(element, animation);

        if(melee != -1)
            equipItem(element, melee);

        if(ranged != -1)
            equipItem(element, ranged);
            
        if(armor != -1)
            equipItem(element, armor);

        setPlayerVisual(element, bodyModel, bodyTxt, headModel, headTxt);

        if(weaponMode)
            setPlayerWeaponMode(element, WEAPONMODE_1HS)
        else
            setPlayerWeaponMode(element, WEAPONMODE_NONE)

        setPlayerCollision(element, false);
    }

    function setWeaponMode(value)
    {
        weaponMode = value;

        if(value)
            setPlayerWeaponMode(element, WEAPONMODE_1HS)
        else
            setPlayerWeaponMode(element, WEAPONMODE_NONE)
    }

    function unspawn() {
        spawned = false;
    }
}

function removeHelper(pid) {
    if(id, pid in pidtohelpers)
        pidtohelpers.rawdelete(id);
}

