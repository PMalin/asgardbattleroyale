
class createBoss extends Bot
{
    constructor(id, streamer, name, posx, posy, posz, angle)
    {
        base.constructor(id, streamer, name);

        setBotPosition(id, posx, posy, posz);    
        setBotAngle(id, angle);
    }

    function spawn() {
        spawned = true;
        local name = getPlayerName(element);

        setPlayerInstance(element, createBoss.getInstanceFromName(name));

        setPlayerMaxHealth(element,maxHealth);
        setPlayerHealth(element,health);

        switch(name)
        {
            case "Xardas":
                setPlayerVisual(element, "Hum_Body_Naked0", 1, "Hum_Head_Thief", 55);
                equipItem(element, Items.id("ITAR_XARDAS"));
            break;
            case "Beliar":
                setPlayerVisual(element, "Hum_Body_Naked0", 1, "Hum_Head_Thief", 55);
                equipItem(element, Items.id("ITAR_DEMENTOR"));
            break;
            case "Vatras":
                setPlayerVisual(element, "Hum_Body_Naked0", 1, "Hum_Head_Thief", 55);
                equipItem(element, Items.id("ITAR_KDF_H"));
            break;
            case "Greg":
                setPlayerVisual(element, "Hum_Body_Naked0", 1, "Hum_Head_Thief", 12);
                equipItem(element, Items.id("ITAR_PIR_H_ADDON"));
                equipItem(element, Items.id("ITMW_KRUMMSCHWERT"));
            break;
            case "Kruk":
                setPlayerVisual(element, "Hum_Body_Naked0", 1, "Hum_Head_Thief", 6);
                equipItem(element, Items.id("ITAR_RAVEN_ADDON"));
                equipItem(element, Items.id("ITMW_BELIARWEAPON_1H_01"));
            break;
        }

        local scale = createBoss.getScaleFromName(name);
        setPlayerScale(element, scale.x,scale.y,scale.z)

        setPlayerAngle(element, angle);
        setPlayerPosition(element, position.x, position.y, position.z);

        if(animation != "STOP")
            playAni(element, animation);
    }

    function bossAttack(typeAttack, targetId) {
        switch(getPlayerName(element))
        {
            case "Troll": trollAttack(typeAttack, targetId); break;
            case "Czarny Troll": trollAttack(typeAttack, targetId); break;
            case "Xardas": xardasAttack(typeAttack, targetId); break;
            case "Beliar": xardasAttack(typeAttack, targetId); break;
            case "Szaman": szamanAttack(typeAttack, targetId); break;
            case "Vatras": vatrasAttack(typeAttack, targetId); break;
            case "Kruk": xardasAttack(typeAttack, targetId); break;
            case "Greg": szamanAttack(typeAttack, targetId); break;
        }
    }

    function vatrasAttack(typeAttack, targetId)
    {
        switch(typeAttack)
        {
            case BotBossAttack.Range:
                playAni(element, "T_FBTSHOOT_2_STAND");
                attackPlayerWithEffect(targetId, element, DAMAGE_MAGIC, 100, true, 1, "spellFX_Icebolt");
            break;
            case BotBossAttack.RangeAdditional:
                playAni(element, "T_FBTSHOOT_2_STAND");
                attackPlayerWithEffect(targetId, element, DAMAGE_MAGIC, 100, true, 1, "spellFX_IceCube");
            break;
            case BotBossAttack.Melee:
                playAni(element, "T_FBTSHOOT_2_STAND");
                attackPlayerWithEffect(targetId, element, DAMAGE_MAGIC, 100, true, 1, "spellFX_Icebolt");
            break;
            case BotBossAttack.MeleeAdditional:
                EffectRegister.RoundVatras(botId, position, element);
            break;
        }
    }

    function szamanAttack(typeAttack, targetId)
    {
        switch(typeAttack)
        {
            case BotBossAttack.Range:
                playAni(element, "T_FBTSHOOT_2_STAND");
                attackPlayerWithEffect(targetId, element, DAMAGE_MAGIC, 100, true, 1, "spellFX_InstantFireball");
            break;
            case BotBossAttack.RangeAdditional:
                playAni(element, "T_FBTSHOOT_2_STAND");
                attackPlayerWithEffect(targetId, element, DAMAGE_MAGIC, 100, true, 1, "spellFX_Firebolt");
            break;
            case BotBossAttack.Melee:
                playAni(element, "T_FBTSHOOT_2_STAND");
                attackPlayerWithEffect(targetId, element, DAMAGE_MAGIC, 100, true, 1, "spellFX_Firebolt");
            break;
            case BotBossAttack.MeleeAdditional:
                EffectRegister.RoundSzaman(botId, position, element);
            break;
        }
    }

    function xardasAttack(typeAttack, targetId)
    {
        switch(typeAttack)
        {
            case BotBossAttack.Range:
                playAni(element, "T_FBTSHOOT_2_STAND");
                attackPlayerWithEffect(targetId, element, DAMAGE_MAGIC, 100, true, 1, "spellFX_Pyrokinesis");
            break;
            case BotBossAttack.RangeAdditional:
                playAni(element, "T_FBTSHOOT_2_STAND");
                attackPlayerWithEffect(targetId, element, DAMAGE_MAGIC, 100, true, 1, "spellFX_SuckEnergy");
            break;
            case BotBossAttack.Melee:
                playAni(element, "T_FBTSHOOT_2_STAND");
                attackPlayerWithEffect(targetId, element, DAMAGE_MAGIC, 100, true, 1, "spellFX_Pyrokinesis");
            break;
            case BotBossAttack.MeleeAdditional:
                EffectRegister.RoundXardas(botId, position, element);
            break;
        }
    }

    function trollAttack(typeAttack, targetId)
    {
        switch(typeAttack)
        {
            case BotBossAttack.Range:
                playAni(element, "T_WARN");
                EffectRegister.Throw(botId, position, getPlayerPosition(targetId));
            break;
            case BotBossAttack.RangeAdditional:
                playAni(element, "T_WARN");
                EffectRegister.Mountain(botId, position, getPlayerPosition(targetId));
            break;
            case BotBossAttack.Melee:
                if(heroId == targetId) 
                    attackPlayer(getIndexBots()[botId], targetId, ATTACK_FRONT);
                else
                    playAni(getIndexBots()[botId], "S_FISTATTACK");     
            break;
            case BotBossAttack.MeleeAdditional:
                playAni(element, "T_WARN");
            break;
        }
    }

    function unspawn() {
        spawned = false;
    }

    static function getScaleFromName(name) {
        switch(name)
        {
            case "Troll":return {x = 1.3, y = 1.3, z = 1.3};
            case "Beliar":return {x = 1.3, y = 1.3, z = 1.3};
            case "Czarny Troll":return {x = 1.5, y = 1.5, z = 1.5};
        }
        return {x = 1.0, y = 1.0, z = 1.0};
    }

    static function getInstanceFromName(name) {
        switch(name)
        {
            case "Czarny Troll":return "TROLL_BLACK";
            case "Troll":return "TROLL";
            case "Szaman":return "ORCSHAMAN_SIT";
        }
        return "PC_HERO";
    }

    static function hitPlayer(botId) {
        local packet = Packet();
        packet.writeUInt8(Packets.Bots);
        packet.writeUInt8(BotPackets.Attack);
        packet.writeUInt8(1);
        packet.writeInt16(botId);
        packet.send(RELIABLE_ORDERED);        
    }
}
