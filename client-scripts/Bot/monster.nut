
class createMonster extends Bot
{
    constructor(id, streamer, name, posx, posy, posz, angle)
    {
        base.constructor(id, streamer, name);

        setBotPosition(id, posx, posy, posz);    
        setBotAngle(id, angle);
    }

    function spawn() {
        spawned = true;
        local name = getPlayerName(element);

        setPlayerInstance(element, createMonster.getInstanceFromName(name));

        setPlayerMaxHealth(element,maxHealth);
        setPlayerHealth(element,health);

        local scale = createMonster.getScaleFromName(name);
        setPlayerScale(element, scale.x,scale.y,scale.z)

        setPlayerAngle(element, angle);
        setPlayerPosition(element, position.x, position.y, position.z);

        if(animation != "STOP")
            playAni(element, animation);
    }

    function unspawn() {
        spawned = false;
    }


    static function getScaleFromName(name) {
        switch(name)
        {
            case "Wielki Wilk":return {x = 1.3, y = 1.3, z = 1.3};
            case "Wielka Bestia":return {x = 1.3, y = 1.3, z = 1.3};
            case "Wielki Aligator":return {x = 1.5, y = 1.5, z = 1.5};
        }
        return {x = 1.0, y = 1.0, z = 1.0};
    }

    static function getInstanceFromName(name) {
        switch(name)
        {
            case "Wilk":return "WOLF";
            case "Wielki Wilk":return "WOLF";
            case "Ognisty Zebacz": return "DRAGONSNAPPER";
            case "Polna Bestia": return "YGIANT_BUG";
            case "Scierwojad":return "SCAVENGER";
            case "Golem Ognisty":return "FIREGOLEM";
            case "Golem":return "STONEGOLEM";
            case "Aligator":return "ALLIGATOR";
            case "Puma":return "STONEPUMA";
            case "Bestia":return "STONEPUMA";
            case "Wielka Bestia":return "SHADOWBEAST";
            case "Trute�":return "SWAMPDRONE";
            case "Topielec":return "LURKER";
            case "Skurwysyn":return "STONEGUARDIAN";
            case "Wielki Aligator": return "ALLIGATOR";
            case "M�ody Wilk": return "YWOLF";
            case "�nie�ny wilk": return "ICEWOLF";
            case "Kretoszczur": return "MOLERAT";
            case "Szczur": return "GIANT_RAT";
            case "Pe�zacz": return "MINECRAWLER";
            case "Silny �cierwojad": return "SCAVENGER_DEMON";
            case "Waran": return "WARAN";
        }
        return "PC_HERO";
    }
}
