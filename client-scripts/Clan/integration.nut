
addPacketListener(Packets.Clan, function(packet)
{
    switch(packet.readUInt8())
    {
        case ClanPackets.Create:
            Clan.register(packet.readInt16(), packet.readString(), packet.readInt16(), packet.readInt16(), packet.readInt16())
        break;
        case ClanPackets.Destroy:
            Clan.destroy(packet.readInt16());

            if(Player.GUI == PlayerGUIEnum.ClanInfo)
                ClanDashboard.hide();
        break;
        case ClanPackets.AddMember:
            Clan.addMember(packet.readInt16(), packet.readInt16(), packet.readUInt8());
        break;
        case ClanPackets.RemoveMember:
            Clan.removeMember(packet.readInt16());
        break;
        case ClanPackets.Info:
            local data = {};

            data = {
                name = "",
                points = 0,
                permission = 0,
                users = [],
                requests = [],
            }
 
            data.name = packet.readString();
            data.points = packet.readInt16();
            data.permission = packet.readUInt8();

            local usersCount = packet.readInt16();
            for(local i = 0; i < usersCount; i ++)
                data.users.append(packet.readString());

            local requestsCount = packet.readInt16();
            for(local i = 0; i < requestsCount; i ++)
                data.requests.append({id = packet.readInt16(), name = packet.readString()});

            ClanDashboard.change(data);
        break;
    }
})