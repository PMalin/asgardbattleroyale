
ClanDashboard <- {};

local activeClanId = -1;

local window = GUI.Window(anx(Resolution.x/2 - 306), any(Resolution.y/2 - 350), anx(612), any(720), "MAIN_ENDLESS.TGA", null, false);
local button = GUI.Button(anx(30), any(-30), anx(552), any(90), "INPUT_ENDLESS.TGA", "", window);
local drawPoints = GUI.Draw(anx(50), any(100), "", window);
GUI.Draw(anx(50), any(130), "U�ytkownicy", window);

local usersList = GUI.GridList(anx(30), any(150), anx(552), any(250), "SLOT_ENDLESS.TGA", "VERTICAL_BAR_ENDLESS.TGA", "THUMB_VERTICAL_ENDLESS.TGA", "INPUT_ENDLESS.TGA", "INPUT_ENDLESS.TGA", window);
local userColumn = usersList.addColumn("", anx(500), Align.Left);
usersList.setMarginPx(35,65,35,65);

GUI.Draw(anx(50), any(400), "Podania", window);

local requestsList = GUI.GridList(anx(30), any(420), anx(552), any(250), "SLOT_ENDLESS.TGA", "VERTICAL_BAR_ENDLESS.TGA", "THUMB_VERTICAL_ENDLESS.TGA", "INPUT_ENDLESS.TGA", "INPUT_ENDLESS.TGA", window);
local requestColumn = requestsList.addColumn("", anx(500), Align.Left);
requestsList.setMarginPx(35,65,35,65);

local joinClan = GUI.Button(anx(30), any(40), anx(200), any(60), "BUTTON_ENDLESS.TGA", "Do��cz", window);
local exitClan = GUI.Button(anx(30), any(40), anx(200), any(60), "BUTTON_ENDLESS.TGA", "Odejd�", window);

local windowToOperateUser = GUI.Window(anx(Resolution.x/2 - 240), any(Resolution.y/2 - 200), anx(480), any(350), "MAIN_ENDLESS.TGA", null, false);
local buttonSaveOperateUser = GUI.Button(anx(100), any(50), anx(280), any(80), "INPUT_ENDLESS.TGA", "Usu�", windowToOperateUser);
local buttonCloseOperateUser = GUI.Button(anx(100), any(200), anx(280), any(80), "INPUT_ENDLESS.TGA", "Zamknij", windowToOperateUser);
local windowToOperateRequest = GUI.Window(anx(Resolution.x/2 - 240), any(Resolution.y/2 - 200), anx(400), any(350), "MAIN_ENDLESS.TGA", null, false);
local buttonSaveRequest = GUI.Button(anx(100), any(50), anx(280), any(80), "INPUT_ENDLESS.TGA", "Akceptuj", windowToOperateRequest);
local buttonCloseRequest = GUI.Button(anx(100), any(200), anx(280), any(80), "INPUT_ENDLESS.TGA", "Zamknij", windowToOperateRequest);
local operatedUser = -1, operatedRequest = -1;

local permission = 0;

function ClanDashboard::show(clanId = activeClanId) {
    Player.GUI = PlayerGUIEnum.ClanInfo;
    activeClanId = clanId;
    BaseGUI.show();
    DesktopGameGUI.hideOut();

    window.setVisible(true);
    requestsList.setVisible(false);

    local packet = Packet();
    packet.writeUInt8(Packets.Clan);
    packet.writeUInt8(ClanPackets.Info);
    packet.writeInt16(clanId);
    packet.send(RELIABLE_ORDERED);
}

function ClanDashboard::hide()
{
    BaseGUI.hide();

    local rowsCount = usersList.rows.len()
    for(local i = 0; i < rowsCount; i ++)
        usersList.removeRow(0);

    rowsCount = requestsList.rows.len()
    for(local i = 0; i < rowsCount; i ++)
        requestsList.removeRow(0);

    DesktopGameGUI.showOut();

    Player.GUI = false;
    window.setVisible(false);
    windowToOperateRequest.setVisible(false);
    windowToOperateUser.setVisible(false);
}

/**
data:
    name = "",
    points = 0,
    permission = 0,
    users = [],
    requests = [],
*/

function ClanDashboard::change(data) {
    permission = data.permission;

    button.setText(data.name);
    drawPoints.setText("Punkty: " + data.points + " U�ytkownik�w " + data.users.len());

    foreach(user in data.users)
        usersList.addRow(user);

    foreach(request in data.requests)
        requestsList.addRow(request.id + "-"+request.name);

    switch(data.permission)
    {
        case 0:
            requestsList.setVisible(false);
            if(getPlayerClan(heroId) == -1)
                joinClan.setVisible(true);
            else
                joinClan.setVisible(false);

            exitClan.setVisible(false);
        break;
        case 1: case 2:
            requestsList.setVisible(true);
            exitClan.setVisible(true);
            joinClan.setVisible(false);
        break;
    }
}

bindKey(KEY_ESCAPE, ClanDashboard.hide, PlayerGUIEnum.ClanInfo)

addEventHandler("GUI.onClick", function(self)
{
    if(Player.GUI != PlayerGUIEnum.ClanInfo)
        return;

    if(buttonCloseOperateUser == self) {
        windowToOperateUser.setVisible(false);
        window.setVisible(true);
    }else if(buttonCloseRequest == self) {
        windowToOperateRequest.setVisible(false);
        window.setVisible(true);
    }else if(buttonSaveOperateUser == self) {
        local packet = Packet();
        packet.writeUInt8(Packets.Clan);
        packet.writeUInt8(ClanPackets.RemoveMember);
        packet.writeInt16(activeClanId);
        packet.writeString(operatedUser);
        packet.send(RELIABLE_ORDERED);

        ClanDashboard.hide();
        ClanDashboard.show();
    }else if(buttonSaveRequest == self) {
        local packet = Packet();
        packet.writeUInt8(Packets.Clan);
        packet.writeUInt8(ClanPackets.AddMember);
        packet.writeInt16(activeClanId);
        packet.writeString(operatedRequest);
        packet.send(RELIABLE_ORDERED);

        ClanDashboard.hide();
        ClanDashboard.show();
    }else if(exitClan == self)
    {
        local packet = Packet();
        packet.writeUInt8(Packets.Clan);
        packet.writeUInt8(ClanPackets.RemoveMember);
        packet.writeInt16(activeClanId);
        packet.writeString(getPlayerName(heroId));
        packet.send(RELIABLE_ORDERED);

        ClanDashboard.hide();
    }else if(joinClan == self)
    {
        local packet = Packet();
        packet.writeUInt8(Packets.Clan);
        packet.writeUInt8(ClanPackets.JoinClan);
        packet.writeInt16(activeClanId);
        packet.writeInt16(heroId);
        packet.send(RELIABLE_ORDERED);

        ClanDashboard.hide();
    }

	if (!(self instanceof GUI.GridListCell))
		return;

    if(permission != 2)
        return;

	local row = self.parent
	local gridlist = row.parent

	if (gridlist == usersList)
	{
        window.setVisible(false);
        windowToOperateUser.setVisible(true);
        operatedUser = self.getText();
    }else if(gridlist == requestsList)
    {
        window.setVisible(false);
        windowToOperateRequest.setVisible(true);
        operatedRequest = self.getText();
    }
});