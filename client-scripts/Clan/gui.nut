
ClanGUI <- {};

ClanGUI.List <- [];
ClanGUI.ActiveId <- -1;

local window = GUI.Window(anx(Resolution.x/2 - 306), any(Resolution.y/2 - 350), anx(612), any(720), "MAIN_ENDLESS.TGA", null, false);
local button = GUI.Button(anx(30), any(-30), anx(552), any(90), "INPUT_ENDLESS.TGA", "Utw�rz klan", window);
local scroll = GUI.ScrollBar(anx(540), any(50), anx(30), any(600), "VERTICAL_BAR_ENDLESS.TGA", "THUMB_VERTICAL_ENDLESS.TGA", "INPUT_ENDLESS.TGA", "INPUT_ENDLESS.TGA", Orientation.Vertical, window);

local picker = GUI.Button(anx(60), any(100), anx(384), any(256), "PICKER_ENDLESS.TGA", "", window);
local pickColorDraw = GUI.Draw(anx(60), any(60), "Wybierz kolor:", window);
local rbutton = GUI.Button(anx(60), any(400), anx(80), any(70), "BUTTON_ENDLESS.TGA", "255", window);
local gbutton = GUI.Button(anx(160), any(400), anx(80), any(70), "BUTTON_ENDLESS.TGA", "255", window);
local bbutton = GUI.Button(anx(260), any(400), anx(80), any(70), "BUTTON_ENDLESS.TGA", "255", window);
local nameinput = GUI.Input(anx(60), any(490), anx(400), any(70), "INPUT_ENDLESS.TGA", "FONT_OLD_10_WHITE_HI.TGA", Input.Text, Align.Left, "", 30, window);
local savebutton = GUI.Button(anx(60), any(580), anx(300), any(70), "BUTTON_ENDLESS.TGA", "Zapisz", window);

function ClanGUI::show() {
    Player.GUI = PlayerGUIEnum.ClanList;

    BaseGUI.show();
    DesktopGameGUI.hideOut();

    window.setVisible(true);

    picker.setVisible(false);
    rbutton.setVisible(false);
    gbutton.setVisible(false);
    bbutton.setVisible(false);
    nameinput.setVisible(false);
    savebutton.setVisible(false);
    pickColorDraw.setVisible(false);

    ClanGUI.change();    
}

function ClanGUI::hide() {
    BaseGUI.hide();

    DesktopGameGUI.showOut();

    Player.GUI = false;
    window.setVisible(false); 

    picker.setVisible(false);
    rbutton.setVisible(false);
    gbutton.setVisible(false);
    bbutton.setVisible(false);
    nameinput.setVisible(false);
    savebutton.setVisible(false);
    pickColorDraw.setVisible(false);

    foreach(item in ClanGUI.List)
        item.setVisible(false);

    ClanGUI.List.clear();
}

bindKey(KEY_ESCAPE, ClanGUI.hide, PlayerGUIEnum.ClanList)

function ClanGUI::change() {
    local scrollIndex = scroll.getValue();

    foreach(item in ClanGUI.List)
        item.setVisible(false);

    ClanGUI.List.clear();

    local showedIndex = 0;
    foreach(index, clan in Clan.List)
    {
        if(index >= scrollIndex && index < scrollIndex + 8)
        {   
            local button = GUI.Button(anx(Resolution.x/2 - 200), any(Resolution.y/2 - 280 + showedIndex * 80), anx(400), any(80), "BUTTON_ENDLESS.TGA", clan.name);
            showedIndex = showedIndex + 1;
            button.setVisible(true);
            ClanGUI.List.append(button);
        }
    }
}

function ClanGUI::createClan() {
    foreach(item in ClanGUI.List)
        item.setVisible(false);

    picker.setVisible(true);
    rbutton.setVisible(true);
    gbutton.setVisible(true);
    bbutton.setVisible(true);
    nameinput.setVisible(true);
    savebutton.setVisible(true);
    pickColorDraw.setVisible(true);
    scroll.setVisible(false);
}


function ClanGUI::selectColor() {
    local posCursor = getCursorPosition();
    local posButton = picker.getPosition();
    local sizeButton = picker.getSize();

    local uPickedXPercent = ((posCursor.x - posButton.x) * 100)/sizeButton.width;
    local uPickedYPercent = ((posCursor.y - posButton.y) * 100)/sizeButton.height;

    local colorR = 255, colorG = 255, colorB = 255;
    if(uPickedXPercent < 10)
    {
        colorR = 200;
        colorG = abs(0 + uPickedYPercent * 1.2);
        colorB = abs(50 + uPickedXPercent*10 + uPickedYPercent/2);
    }else if(uPickedXPercent < 20)
    {
        colorR = 200 - (5 * uPickedXPercent);
        colorG = abs(0 + uPickedYPercent * 1.2);
        colorB = 200 - (20 - uPickedXPercent);
    }else if(uPickedXPercent < 30)
    {
        colorR = 100 - (3 * uPickedXPercent) + uPickedYPercent;
        colorG = abs(4 + uPickedYPercent * 1.2);
        colorB = 200;
    }else if(uPickedXPercent < 40)
    {
        colorR = abs(1 + uPickedYPercent * 1.2);
        colorG = abs(10 + (uPickedXPercent - 30) * 10) + abs(uPickedYPercent * 1.2);
        colorB = 200;
    }else if(uPickedXPercent < 50)
    {
        colorR = abs(4 + uPickedYPercent * 1.2);
        colorG = abs(120 + (uPickedXPercent - 40) * 5) + abs(uPickedYPercent * 1.2);
        colorB = 200;
    }else if(uPickedXPercent < 50)
    {
        colorR = abs(4 + uPickedYPercent * 1.2);
        colorG = abs(120 + (uPickedXPercent - 40) * 5) + abs(uPickedYPercent * 1.2);
        colorB = 200;
    }else if(uPickedXPercent < 60)
    {
        colorR = abs(4 + uPickedYPercent * 1.2);
        colorG = 200;
        colorB = abs(180 - (uPickedXPercent - 50) * 13) + abs(uPickedYPercent * 1.2);
    }else if(uPickedXPercent < 70)
    {
        colorR = abs(0 + (uPickedXPercent - 60) * 5) + abs(uPickedYPercent * 1.2);
        colorG = 200;
        colorB = abs(4 + uPickedYPercent * 1.2);
    }else if(uPickedXPercent < 80)
    {
        colorR = abs(55 + (uPickedXPercent - 70) * 10) + abs(uPickedYPercent * 1.2);
        colorG = 200;
        colorB = abs(4 + uPickedYPercent * 1.2);
    }else{
        colorR = 200;
        colorG = abs(200 - (uPickedXPercent - 80) * 10) + abs(uPickedYPercent * 1.2);
        colorB = abs(4 + uPickedYPercent * 1.2);
    }
    pickColorDraw.setColor(colorR, colorG, colorB);

    rbutton.setText(colorR);
    gbutton.setText(colorG);
    bbutton.setText(colorB);

}

addEventHandler("GUI.onClick", function(self)
{
    if(Player.GUI != PlayerGUIEnum.ClanList)
        return;

    if(self == picker)
    {
        ClanGUI.selectColor();
        return;
    }

    if(self == button)
    {
        ClanGUI.createClan();
        return;
    }

    if(self == savebutton)
    {
        if(nameinput.getText().len() > 2) {
            local packet = Packet();
            packet.writeUInt8(Packets.Clan);
            packet.writeUInt8(ClanPackets.Create);
            packet.writeString(nameinput.getText());
            packet.writeInt16(rbutton.getText().tointeger());
            packet.writeInt16(gbutton.getText().tointeger());
            packet.writeInt16(bbutton.getText().tointeger());
            packet.send(RELIABLE_ORDERED);   
            ClanGUI.hide();
        }
        return;
    }

    foreach(indexItem, listItem in ClanGUI.List)
    {
        if(listItem == self)
        {
            local idClan = getClanByName(listItem.getText());
            ClanGUI.ActiveId = idClan;
            ClanGUI.hide();
            ClanDashboard.show(ClanGUI.ActiveId);

            foreach(item in ClanGUI.List)
                item.setVisible(false);

            ClanGUI.List.clear();
            return;
        }
    }
});

addEventHandler("GUI.onChange", function (self) {
    if(Player.GUI != PlayerGUIEnum.ClanList)
        return;

    if( self == scroll )
        ClanGUI.change()
})

