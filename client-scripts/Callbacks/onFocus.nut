local focusPlayer = -1;

addEventHandler("onFocus", function (new, old) {
    focusPlayer = new;
})

function getPlayerFocus(pid = heroId) {
    if(pid == heroId)
        return focusPlayer;

    return -1;
}