

class _PlayerItem
{
    equpiedItems = null;

    constructor() {
        equpiedItems = [];
    }

    function useItem(instance, amount = 1)
    {
        local itemId = Items.id(instance)

        if (itemId == -1)
            return;

        instance = ItemIntegration.getItem(Items.id(instance));
        if(!instance)
            return;

        ItemIntegration.packetUseItem(itemId, amount);
    }

    function unEquipItemTable(itemId)
    {
        foreach(_i, _item in equpiedItems)
        {
            if(itemId == _item)
                equpiedItems.remove(_i);
        }
    }

    function equipItemTable(itemId)
    {
        equpiedItems.append(itemId);
    }

    function findInEquipment(itemId)
    {
        foreach(_item in equpiedItems)
        {
            if(itemId == _item)
                return true;
        }
        return false;
    }
}

PlayerItem <- _PlayerItem();
