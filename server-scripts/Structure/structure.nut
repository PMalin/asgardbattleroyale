local All = [];
local lastId = 0;

class Structure
{
    id = -1;
    bType = -1;

    health = 1;
    maxHealth = 1;

    owner = -1;
    position = null;

    constructor(Type, x,y,z, angle, _owner = -1)
    {
        bType = Type;

        position = {x = x,y = y,z = z,angle = angle};
        owner = _owner;

        switch(Type)
        {
            case StructureType.Locket: case StructureType.Stairs:
                health = 10;
                maxHealth = 10;
            break;
            case StructureType.Bush:
                health = 3;
                maxHealth = 3;
            break;
            case StructureType.WoodenWall:
                health = 35;
                maxHealth = 35;
            break;
            case StructureType.Wall: case StructureType.WallHorizontal:
                health = 70;
                maxHealth = 70;
            break;
            case StructureType.Krate:
                health = 25;
                maxHealth = 25;
            break;
            case StructureType.Bonus:
                health = 3;
                maxHealth = 3;
            break;
        }

        id = Structure.getFreeId();

        synchronize();

        All.append(this);
    }

    function synchronize(pid = -1)
    {
        local packet = Packet(Packets.Structure);

        packet.writeUInt8(StructurePackets.Add);
        packet.writeInt16(id);
        packet.writeInt16(owner);
        packet.writeInt16(health);
        packet.writeInt16(maxHealth);
        packet.writeInt8(bType);
        packet.writeFloat(position.x);
        packet.writeFloat(position.y);
        packet.writeFloat(position.z);
        packet.writeInt16(position.angle);
        if(pid == -1)
            packet.sendToAll(RELIABLE_ORDERED);
        else
            packet.send(pid, RELIABLE_ORDERED);
    }

    function remove()
    {
        local packet = Packet(Packets.Structure);

        packet.writeUInt8(StructurePackets.Remove);
        packet.writeInt16(id);
        packet.sendToAll(RELIABLE_ORDERED);

        foreach(_ind, _structure in All)
        {
            if(_structure.id == id)
                All.remove(_ind);
        }
    }

    function onGetHit(pid)
    {
        health --;
        if(health <= 0) {
            switch(bType)
            {
                case StructureType.Bonus:
                    local randomBonus = rand() % 4;

                    switch(randomBonus)
                    {
                        case PlayerBonus.Defense:
                            smallNoteToAll(getPlayerName(pid) + " uzyska� bonus. (Obrona | 5 minut)");
                            givePlayerBonus(pid, PlayerBonus.Defense)
                        break;
                        case PlayerBonus.DefenseSmall:
                            smallNoteToAll(getPlayerName(pid) + " uzyska� bonus. (Obrona | 3 minuty)");
                            givePlayerBonus(pid, PlayerBonus.DefenseSmall)
                        break;
                        case PlayerBonus.Damage:
                            smallNoteToAll(getPlayerName(pid) + " uzyska� bonus. (Obra�enia | 5 minut)");
                            givePlayerBonus(pid, PlayerBonus.Damage)
                        break;
                        default:
                            smallNoteToAll(getPlayerName(pid) + " uzyska� bonus. (Obra�enia | 3 minuty)");
                            givePlayerBonus(pid, PlayerBonus.DamageSmall)
                        break;
                    }
                break;

                case StructureType.Locket:
                    local tab = [
                        ["ITMW_2H_SPECIAL_04",1],
                        ["ITAR_RANGER_ADDON",1],
                        ["ITMW_2H_PAL_SWORD",1],
                        ["ITRW_CROSSBOW_H_01",1],
                        ["ITAR_SLD_M",1],
                        ["ITMW_2H_SLD_SWORD",1],
                        ["ITMW_2H_SLD_AXE",1],
                        ["ITRW_BOLT",20],
                        ["ITMW_ZWEIHAENDER4",1],
                        ["ITRW_ARROW",20],
                        ["ITAR_CORANGAR",1],
                        ["ITMW_1H_SPECIAL_04",1],
                        ["ITRW_CROSSBOW_L_01",1],
                        ["ITMW_1H_BLESSED_01",1],
                        ["ITAR_KDW_H",1],
                        ["ITRU_THUNDERBALL",1],
                        ["ITAR_THORUS_ADDON",1],
                        ["ITAR_BLOODWYN_ADDON",1],
                        ["ITMW_SCHWERT3",1],
                        ["ITRW_CROSSBOW_M_01",1],
                        ["ITRU_WINDFIST",1],
                        ["ITRU_CONCUSSIONBOLT",1],
                        ["ITRW_BOW_H_03",1]
                    ]

                    for(local i = 0; i <= DROP_AMOUNT_FROM_LOCKET; i++)
                    {
                        local item = tab[rand() % tab.len()];
                        ItemInGroundBucket(Items.id(item[0]), item[1], position.x - rand() % 60, position.y, position.z + rand() % 60 )
                    }
                break;
                case StructureType.WoodenWall:
                break;
                case StructureType.Wall:
                break;
                case StructureType.Krate:
                break;
            }

            callEvent("onPlayerDestroyStructure", pid, this);
            remove();
            return;
        }

        local packet = Packet(Packets.Structure);

        packet.writeUInt8(StructurePackets.Hit);
        packet.writeInt16(id);
        packet.writeInt16(health);
        packet.sendToAll(RELIABLE_ORDERED);
    }

    static function onStartGame() {
        switch(Game.activeWorld)
        {
            case Worlds.Jarkendar:
                Structure(StructureType.Locket,-2752.27, -750.781, -6753.75,40);
                Structure(StructureType.Locket,-7097.89, -1088.36, -5910.86,40);
                Structure(StructureType.Locket,-15546.1, -438.359, -4990.7,40);
                Structure(StructureType.Locket,-23092.6, -2846.25, -3737.42,40);
                Structure(StructureType.Locket,-23219.6, -3361.56, -12077.3,40);
                Structure(StructureType.Locket,179.922, -952.656, -6184.22,40);
                Structure(StructureType.Locket,14272.6, -4748.13, -2051.02,40);
                Structure(StructureType.Locket,25280.2, -4942.03, 6459.14,40);
                Structure(StructureType.Locket,29169.2, -4428.98, 9788.13,40);
                Structure(StructureType.Locket,28008.8, -3334.14, 16148.3,40);
                Structure(StructureType.Locket,11590.5, -5020.31, 13210.9,40);
                Structure(StructureType.Locket,-667.969, -2798.28, 14490.5,40);
                Structure(StructureType.Locket,-6062.73, -2917.19, 23170.7,40);
                Structure(StructureType.Locket,-26060.2, -777.813, 13865.1,40);
                Structure(StructureType.Locket,-35126.8, -1873.36, 19046,40);
                Structure(StructureType.Locket,-28502.5, -1957.97, 26656,40);
                Structure(StructureType.Locket,-14611.3, -3906.25, -4755.39,40);
            break;
            case Worlds.Khorinis:
                Structure(StructureType.Locket,4662,848,7080,150.863);
                Structure(StructureType.Locket,9959,368,-701,261.174);
                Structure(StructureType.Locket,13831,1791,-15319,236.893);
                Structure(StructureType.Locket,9836,365,4651,348.298);
                Structure(StructureType.Locket,-564,2439,15655,119.995);
                Structure(StructureType.Locket,17539,2250,28898,120.85);
                Structure(StructureType.Locket,28713,3224,28690,141.161);
                Structure(StructureType.Locket,37101,7079,32116,27.8297);
                Structure(StructureType.Locket,51847,8017,34392,38.5783);
                Structure(StructureType.Locket,59493,6931,40722,339.97);
                Structure(StructureType.Locket,74492,6773,28965,154.042);
                Structure(StructureType.Locket,78529,5177,22774,228.514);
                Structure(StructureType.Locket,61909,3401,15550,317.74);
                Structure(StructureType.Locket,61909,3401,15550,317.74);
                Structure(StructureType.Locket,38696,3926,-2163,98.7598);
                Structure(StructureType.Locket,48301,2440,-2928,43.7836);
                Structure(StructureType.Locket,59485,2265,-7329,44.6852);
                Structure(StructureType.Locket,73096,3301,-11458,181.844);
                Structure(StructureType.Locket,68738,1769,-24733,200.777);
                Structure(StructureType.Locket,66191,1521,-31674,165.16);
                Structure(StructureType.Locket,84729,4356,-11340,265.753);
                Structure(StructureType.Locket,47730,1707,-10429,334.852);
                Structure(StructureType.Locket,48519,3030,-18139,11.6252);
                Structure(StructureType.Locket,39214,2912,-24050,252.922);
                Structure(StructureType.Locket,48282,4990,19358,122.171);
                Structure(StructureType.Locket,34061,-2263,-42,324.295);
                Structure(StructureType.Locket,3894,799,-19781,109.611);
                Structure(StructureType.Locket,27881,777,-14210,126.849);
                Structure(StructureType.Locket,29857,6082,-15503,51.7643);
            break;

            case Worlds.Dolina:
                Structure(StructureType.Locket,-59160,2584,14280,143.149);
                Structure(StructureType.Locket,-50645,2575,11303,62.0995);
                Structure(StructureType.Locket,-40537,694,9937,142.123);
                Structure(StructureType.Locket,-37516,447,2695,150.327);
                Structure(StructureType.Locket,-35072,3190,-13916,50.3623);
                Structure(StructureType.Locket,-24846,-1317,-10655,93.0776);
                Structure(StructureType.Locket,-13297,-1982,-660,106.071);
                Structure(StructureType.Locket,9656,-397,-3349,346.689);
                Structure(StructureType.Locket,1027,245,-204,290.029);
                Structure(StructureType.Locket,1692,-1037,7185,287.388);
                Structure(StructureType.Locket,-11239,-151,18086,287.509);
                Structure(StructureType.Locket,-27428,-668,21006,271.532);
                Structure(StructureType.Locket,-32205,917,10979,163.688);
                Structure(StructureType.Locket,-27034,-148,2133,86.9479);
                Structure(StructureType.Locket,-14818,906,-25502,193.901);
                Structure(StructureType.Locket,4776,6997,-21727,287.34);
                Structure(StructureType.Locket,23080,891,-8234,143.519);
                Structure(StructureType.Locket,25889,331,-1517,46.1605);
                Structure(StructureType.Locket,23276,1108,-11896,184.236);
                Structure(StructureType.Locket,21211,7662,-35048,220.976);
            break;
        }

    }

    static function getFreeId()
    {
        lastId = lastId + 1;

        return lastId;
    }

    static function onEndGame()
    {
        lastId = 0;

        All.clear();
    }

    static function onPlayerJoin(pid) {
        foreach(struct in getStructures())
            struct.synchronize(pid);
    }
}

function getStructure(id) {
    foreach(indsx, strct in All)
    {
        if(strct.id == id)
            return strct;
    }

    return null;
}
getStructures <- @() All;

