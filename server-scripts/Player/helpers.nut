_setPlayerStrength <- setPlayerStrength;
_setPlayerDexterity <- setPlayerDexterity;
_setPlayerMaxHealth <- setPlayerMaxHealth;
_setPlayerHealth <- setPlayerHealth;
_setPlayerMaxMana <- setPlayerMaxMana;
_setPlayerMana <- setPlayerMana;
_setPlayerVisual <- setPlayerVisual;

function setPlayerStrength(pid, val)
{
	Player[pid].str = val;
	_setPlayerStrength(pid, val);
}

function getPlayerStrength(pid)
{
    return Player[pid].str;
}

function setPlayerDexterity(pid, val)
{
	Player[pid].dex = val;
	_setPlayerDexterity(pid, val);
}

function getPlayerDexterity(pid)
{
    return Player[pid].dex;
}

function setPlayerMaxMana(pid, val)
{
	Player[pid].mana = val;
	_setPlayerMaxMana(pid, val);
}

function getPlayerMaxMana(pid)
{
    return Player[pid].mana;
}

function completeHealth(pid)
{
	setPlayerHealth(pid, getPlayerMaxHealth(pid));
}

function setPlayerHealth(pid, val)
{
	if(pid == -1)
		return;
		
	if(val < 0)
		val = 0;

	Player[pid].hp = val;
	_setPlayerHealth(pid, val);
}

function getPlayerHealth(pid)
{
	return Player[pid].hp;
}

function setPlayerMaxHealth(pid, val)
{
	Player[pid].maxHp = val;
	_setPlayerMaxHealth(pid, val)
}

function getPlayerMaxHealth(pid)
{
    return Player[pid].maxHp;
}

function setPlayerVisual(pid, bodyModel, bodyTxt, headModel, headTxt)
{
	Player[pid].bodyModel = bodyModel;
	Player[pid].bodyTxt = bodyTxt;
	Player[pid].headModel = headModel;
	Player[pid].headTxt = headTxt;

	_setPlayerVisual(pid, bodyModel, bodyTxt, headModel, headTxt);
}

function sendAdditionalInformations(pid) {
	local obj = getPlayer(pid);
    local packet = Packet(Packets.Player);
	packet.writeUInt8(PlayerPackets.AdditionalPackets);
	packet.writeInt16(obj.kills);
	packet.writeInt16(obj.deaths);
	packet.writeInt16(obj.winGames);
	packet.writeInt16(obj.avatarId);
    packet.send(pid, RELIABLE_ORDERED);
}

function addEffect(pid, effect)
{
    local packet = Packet(Packets.Player);
	packet.writeUInt8(PlayerPackets.Effect);
	packet.writeInt16(pid);
	packet.writeString(effect);
    packet.sendToAll(RELIABLE_ORDERED);
}

local VisualAssets = {
	"1" : [1, "Hum_Head_Bald", 0],
	"2" : [1, "Hum_Head_Bald", 67],
	"3" : [1, "Hum_Head_Pony", 87],
	"4" : [1, "Hum_Head_Bald", 36],
	"5" : [1, "Hum_Head_Fighter", 105],
	"6" : [1, "Hum_Head_Bald", 23],
	"7" : [1, "Hum_Head_Bald", 55],
	"8" : [1, "Hum_Head_Fighter", 100],
	"9" : [1, "Hum_Head_FatBald", 70],
	"10" : [2, "Hum_Head_FatBald", 121],
	"11" : [3, "Hum_Head_Psionic", 12],
	"12" : [0, "Hum_Head_Psionic", 108],
	"13" : [0, "Hum_Head_Fighter", 46],
	"14" : [1, "Hum_Head_Fighter", 1],
	"15" : [0, "Hum_Head_Fighter", 67],
	"16" : [1, "Hum_Head_Thief", 14],
	"17" : [1, "Hum_Head_Bald", 60],
	"18" : [1, "Hum_Head_FatBald", 68],
	"19" : [1, "Hum_Head_Bald", 69],
	"20" : [0, "Hum_Head_Bald", 64],
	"21" : [1, "Hum_Head_Bald", 58],
	"22" : [1, "Hum_Head_Psionic", 13],
	"23" : [3, "Hum_Head_Psionic", 28],
	"24" : [1, "Hum_Head_Fighter", 27],
	"25" : [1, "Hum_Head_Thief", 22],
	"26" : [3, "Hum_Head_Thief", 133],
	"27" : [1, "Hum_Head_FatBald", 51],
	"28" : [1, "Hum_Head_Fighter", 120],
	"29" : [1, "Hum_Head_Thief", 91],
	"30" : [1, "Hum_Head_Fighter", 114],
}

function setPlayerVisualFromAccountAvatarId(pid, avatarId) {
	avatarId = avatarId.tostring();

	if(!(avatarId in VisualAssets))
		avatarId = "1";

	local objVisual = VisualAssets[avatarId.tostring()];
	setPlayerVisual(pid, "Hum_Body_Naked0", objVisual[0], objVisual[1], objVisual[2])
}

