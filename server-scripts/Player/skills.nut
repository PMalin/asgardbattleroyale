local MAX_TIMEOUTS = [120, 60, 45, 60, 120];

class _PlayerSkills
{
    pid = -1;

    constructor(_pid)
    {
        pid = _pid;

        timeouts = [120, 60, 45, 60, 120];
        active = [false, false, false, false, false];
    }

    function clear()
    {
        timeouts = [120, 60, 45, 60, 120];
        active = [false, false, false, false, false];
    }

    timeouts = null;
    active = null;
}

PlayerSkills <- [];

for(local i = 0; i<= getMaxSlots(); i++)
    PlayerSkills.append(_PlayerSkills(i));

getPlayerSkills <- @(id) PlayerSkills[id];
getPlayersSkills <- @() PlayerSkills;

setTimer(function () {
    foreach(playerSkill in getPlayersSkills())
    {
        if(getPlayer(playerSkill.pid).inGame)
        {
            foreach(index,timeout in playerSkill.timeouts)
            {
                if(timeout != 0)
                    timeout = timeout - 1;

                if(timeout < 0)
                    timeout = 0;

                PlayerSkills[playerSkill.pid].timeouts[index] = timeout;
            }
            local packet = Packet(Packets.Player);
            packet.writeUInt8(PlayerPackets.UpdateSkill);
            packet.writeInt16(playerSkill.timeouts[0]);
            packet.writeInt16(playerSkill.timeouts[1]);
            packet.writeInt16(playerSkill.timeouts[2]);
            packet.writeInt16(playerSkill.timeouts[3]);
            packet.writeInt16(playerSkill.timeouts[4]);
            packet.send(playerSkill.pid, RELIABLE_ORDERED);
        }
    }
}, 1000, 0);

function resetPlayerSkillTimeouts(pid) {
    PlayerSkills[pid].timeouts = [120, 60, 45, 60, 120];
}

function playerUseSkill(pid, skillId) {
    local obj = getPlayerSkills(pid);

    if(obj.timeouts[skillId] == 0)
    {
        obj.active[skillId] = true;
        obj.timeouts[skillId] = MAX_TIMEOUTS[skillId];

        switch(skillId)
        {
            case PlayerSkill.Flash:
                setPlayerScale(pid, 1.0, 1.0, 2.0);
                setTimer(function(pid) {
                    setPlayerScale(pid, 1.0, 1.0, 1.0);
                }, 5000, 1, pid);
            break;
            case PlayerSkill.Arrows:
                getPlayer(pid).hitMultiplier = 1;
            break;
            case PlayerSkill.Shield:
                getPlayer(pid).shieldBonus = 2;
            break;
            case PlayerSkill.DeathKiss:
                getPlayer(pid).smellyHit = 1;
            break;
            case PlayerSkill.Devil:
                getPlayer(pid).hitBonus = 5;
            break;
        }

        foreach(player in getPlayers())
        {
            if(player.inGame)
            {
                local packet = Packet(Packets.Player);
                packet.writeUInt8(PlayerPackets.UseSkill);
                packet.writeInt16(pid);
                packet.writeUInt8(skillId);
                packet.send(player.id, RELIABLE_ORDERED);
            }   
        }
    }
}

function forceUnusualSkillTimeout(pid, id, value) {
    PlayerSkills[pid].timeouts[id] = value;

    local packet = Packet(Packets.Player);
    packet.writeUInt8(PlayerPackets.ResetSkill);
    packet.writeUInt8(id);
    packet.writeInt16(value);
    packet.send(pid, RELIABLE_ORDERED);
}