_print <- print;

function print(...) {
    foreach(arg in vargv)
    {
        switch(type(arg))
        {
            case "array": case "table":
                foreach(index, value in arg)
                    _print(index + "-" + value);
            break;
            case "string": case "float": case "integer":
                _print(arg);
            break;
            default:
                _print(arg);
            break;
        }
    }
}
