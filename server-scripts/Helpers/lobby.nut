local list = [];

for(local i = 0; i < 10; i++)
    list.append(null);

function clearLobbyQueries()
{
    for(local i = 0; i < 10; i++)
        list[i] = null;
}

function addLobbyQuerie(name) {    
    foreach(index, insert in list)
        if(index != 9)
            list[(9-index)] = list[(8-index)];

    list[0] = name;

    local packet = Packet(Packets.Helpers);
    packet.writeUInt8(HelperPackets.AddLobby);
    packet.writeString(name);
    packet.sendToPlayers(RELIABLE_ORDERED);
}

function onPlayerJoinLobbyQueries(pid) {
    local listReversed = clone list;

    listReversed.reverse();

    foreach(name in listReversed)
    {
        if(name == null)
            continue;

        local packet = Packet(Packets.Helpers);
        packet.writeUInt8(HelperPackets.AddLobby);
        packet.writeString(name);
        packet.send(pid, RELIABLE_ORDERED);
    }
}