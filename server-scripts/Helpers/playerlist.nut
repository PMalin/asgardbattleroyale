local list = [];

for(local i = 0; i <= getMaxSlots(); i++)
    list.append(null);

function getAllPlayers() {
    return list;
}

function getPlayerSlot(pid) {
    return list[pid];
}

function getRandomPlayer() {
    local ids = [];
    foreach(player in getPlayers())
        if(player.inGame && player.isObserver == false)
            ids.append(player.id);

    if(ids.len() == 0)
        return -1;

    return ids[rand() % ids.len()];
}

function getStringPlayersNames()
{
    local names = "";
    foreach(pid, name in list)
        if(name != null)
            names += name + ",";

    if(names.len() > 1)
        names.slice(-1);
        
    return names;
}

function addPlayerToSlot(pid, name) {
    list[pid] = name;

    local packet = Packet(Packets.Helpers);
    packet.writeUInt8(HelperPackets.AddPlayer);
    packet.writeInt16(pid);
    packet.writeString(name);
    packet.sendToPlayers(RELIABLE_ORDERED);

    foreach(playerId, playerName in list)
    {
        if(playerName == null)
            continue;
            
        local packet = Packet(Packets.Helpers);
        packet.writeUInt8(HelperPackets.AddPlayer);
        packet.writeInt16(playerId);
        packet.writeString(playerName);
        packet.send(pid, RELIABLE_ORDERED);
    }

    callEvent("onPlayerGetSlot", pid)
}

function removePlayerFromSlot(pid) {
    list[pid] = null;

    local packet = Packet(Packets.Helpers);
    packet.writeUInt8(HelperPackets.RemovePlayer);
    packet.writeInt16(pid);
    packet.sendToPlayers(RELIABLE_ORDERED);

    callEvent("onPlayerLostSlot", pid)
}

function getDistanceBetweenPlayers(pid1,pid2) {
    local pos1 = getPlayerPosition(pid1);
    local pos2 = getPlayerPosition(pid2);
    
    return getDistance3d(pos1.x,pos1.y,pos1.z,pos2.x,pos2.y,pos2.z);
}

function getDistanceBetweenPositions(pos1, pos2) {
    return getDistance3d(pos1.x,pos1.y,pos1.z,pos2.x,pos2.y,pos2.z);
}