class MakroDetector extends AntyCheatModule
{
    function Write(step, status)
    {
        local playerId = id;

        local exist = false;
        local obj = {player = playerId, step = step, status = status, count = 1};

        foreach(item in data)
        {
            if(item.player == playerId && item.step == step && item.status == status) {
                item.count = item.count + 1;
                exist = true;
                AntyCheatLog.Update("MAKRO", getPlayerSerial(playerId), step, status);
                return;
            }
        }

        if(exist == false)
        {
            data.append(obj);
            AntyCheatLog.Write("MAKRO", getPlayerName(playerId), getPlayerSerial(playerId), step, status, 1);
        }
    }
}