
class AntyCheatLog 
{
    
    data = {
        "MAKRO" : [],
        "TP" : [],
        "STATISTICS" : [],
    };

    function Write(name, ...)
    {
        switch(name)
        {
            case "MAKRO":
                data[name].append({
                    playerName = vargv[0],
                    playerSerial = vargv[1],
                    step = vargv[2],
                    status = vargv[3],
                    count = vargv[4],
                })
            break;
            case "TP":
                data[name].append({
                    playerName = vargv[0],
                    playerSerial = vargv[1],
                    count = vargv[2],
                })
            break;
            case "STATISTICS":
                data[name].append({
                    playerName = vargv[0],
                    playerSerial = vargv[1],
                    sended = vargv[2],
                    original = vargv[3]
                })
            break;
        }
    }

    function Update(name, ...)
    {
        switch(name)
        {
            case "MAKRO":
                foreach(item in data[name])
                    if(item.playerSerial == vargv[0] && item.step == vargv[1] && item.status == vargv[2])
                        item.count = item.count + 1;
            break;
            case "TP":
                foreach(item in data[name])
                    if(item.playerSerial == vargv[0])
                        item.count = item.count + 1;            
            break;
        }       
    }

    function GetMessageForPlayer(serial)
    {
        local messages = [];

        foreach(item in data["MAKRO"])
        {
            if(item.playerSerial == serial)
                messages.append("Makro poziom ("+item.step+") - stopie� zagro�enia "+item.status)
        }
        foreach(item in data["TP"])
        {
            if(item.playerSerial == serial)
                messages.append("Teleportowa� si� w spos�b niedozwolony "+item.count)
        }
        foreach(item in data["STATISTICS"])
        {
            if(item.playerSerial == serial)
                messages.append("Zmieni� statystyki z "+item.original + " na "+item.sended)
        }

        return messages;
    }

    function Save()
    {
        local myfile = file("acclogs/"+mDate.get().str+".txt", "w");
        myfile.write(JSON.encode(AntyCheatLog.data));
        myfile.close();     
    }
}

setTimer(AntyCheatLog.Save, 1000 * 60 * 5, 0);