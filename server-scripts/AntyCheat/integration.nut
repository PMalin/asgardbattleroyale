
addPacketListener(Packets.AntyCheat, function(pid, packet)
{
    switch(packet.readUInt8())
    {
        case AntyCheatPackets.Makro:
            getPlayerAntyCheat(pid).Makro().Write(packet.readUInt8(), packet.readUInt8());
        break;
        case AntyCheatPackets.Tp:
            getPlayerAntyCheat(pid).Tp().Write();
        break;
        case AntyCheatPackets.Statistics:
            getPlayerAntyCheat(pid).Statistics().Write(packet.readInt32());
        break;
    }
})