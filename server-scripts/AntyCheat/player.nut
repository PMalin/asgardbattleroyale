
class _PlayerAntyCheat
{
    id = -1;

    modules = null;

    constructor(pid)
    {
        id = pid;

        modules = {
            makro = MakroDetector(pid),
            tp = TpDetector(pid),
            statistics = StatisticsDetector(pid)
        }   
    }

    function Makro()
    {
        return modules.makro;
    }

    function Tp()
    {
        return modules.tp;
    }

    function Statistics()
    {
        return modules.statistics;
    }

    function clear()
    {
        foreach(module in modules)
            module.clear();
    }
}
PlayerAntyCheat <- [];

for(local i = 0; i<= getMaxSlots(); i++)
    PlayerAntyCheat.append(_PlayerAntyCheat(i));

getPlayerAntyCheat <- @(id) PlayerAntyCheat[id];
getPlayersAntyCheat <- @() PlayerAntyCheat;