
function SortRanking(a, b) {
    if (a.points < b.points) return 1;
    if (a.points > b.points) return -1;
    return 0;
}

function SortOnline(a, b) {
    if (isPlayerSpawned(a.id) && !isPlayerSpawned(b.id)) return 1;
    if (!isPlayerSpawned(a.id) && isPlayerSpawned(b.id)) return -1;
    return 0;
}

function QueryRanking(pagination = 0) {
    local ranking = [];
    local counter = 0;

    local minSlot = pagination * 10;
    local maxSlot = pagination * 10 + 9;

    local players = [];
    foreach(acc in getAllAccounts())
        players.append({ name = acc.name, avatarId = acc.avatarId, points = CountPoints(acc), winGames = acc.winGames, deaths = acc.deaths, kills = acc.kills});

    if(players.len() > 0)
        players.sort(SortRanking);

    foreach(acc in players)
    {
        if(counter >= minSlot && counter <= maxSlot)
            ranking.append(acc);

        if(counter > maxSlot)
            break;

        counter = counter + 1;
    }

    return ranking;
}

function QueryRankingOnline(pagination = 0) {
    local ranking = [];
    local counter = 0;

    local minSlot = pagination * 10;
    local maxSlot = pagination * 10 + 10;

    foreach(player in getPlayers())
    {
        if(player.account == null)
            continue;

        local acc = player.account;
        if(counter >= minSlot && counter < maxSlot)
            ranking.append({ id = player.id, name = getPlayerName(player.id), avatarId = player.avatarId, points = player.CountPoints(), winGames = player.winGames, deaths = player.deaths, kills = player.kills});

        if(counter > maxSlot)
            break;

        counter = counter + 1;
    }

    return ranking;
}

function sendRankingToPlayer(pid, pagination) {
    local ranking = QueryRanking(pagination);

    local packet = Packet(Packets.Ranking);
    packet.writeUInt8(RankingPackets.SendRanking);
    packet.writeInt16(ranking.len());

    foreach(pool in ranking)
    {
        packet.writeString(pool.name);
        packet.writeInt16(pool.avatarId);
        packet.writeInt16(pool.winGames);
        packet.writeInt16(pool.kills);
        packet.writeInt16(pool.deaths);
        packet.writeInt16(pool.points);
    }

    packet.send(pid, RELIABLE_ORDERED);
}

function sendOnlineToPlayer(pid, pagination) {
    local ranking = QueryRankingOnline(pagination);

    local packet = Packet(Packets.Ranking);
    packet.writeUInt8(RankingPackets.SendOnline);
    packet.writeInt16(ranking.len());

    foreach(pool in ranking)
    {
        packet.writeInt16(pool.id);
        packet.writeString(pool.name);
        packet.writeBool(isPlayerSpawned(pool.id));
        packet.writeInt16(pool.avatarId);
        packet.writeInt16(pool.winGames);
        packet.writeInt16(pool.kills);
        packet.writeInt16(pool.deaths);
        packet.writeInt16(pool.points);
    }

    packet.send(pid, RELIABLE_ORDERED);
}