
addPacketListener(Packets.Clan, function(pid, packet)
{
    switch(packet.readUInt8())
    {
        case ClanPackets.Create:
            if(getPlayerClan(pid) != -1)
            {
                smallNote(pid, "Posiadasz ju� klan.");
                return;
            }

            local name = packet.readString();
            local r = packet.readInt16();
            local g = packet.readInt16();
            local b = packet.readInt16();

            local clan = Clan();
            clan.name = name;
            clan.color.r = r;
            clan.color.g = g;
            clan.color.b = b;
            clan.addMember(pid, ClanRank.Leader);
            setPlayerClan(pid, clan.id);
            setPlayerColor(pid, r, g, b);
            clan.send();
            Clan.save();

            smallNoteToAll("Pojawi� si� nowy klan "+name);
        break;
        case ClanPackets.Info:
            local clanId = packet.readInt16();
            
            if(!(clanId in Clan.getAll()))
                return;

            local clan = Clan.get(clanId);
            local packet = Packet(Packets.Clan);
            packet.writeUInt8(ClanPackets.Info);

            packet.writeString(clan.name);
            packet.writeInt16(clan.calculateClanPoints());
            packet.writeUInt8(clan.readPlayerPermission(pid));
            packet.writeInt16(clan.members.len());

            foreach(member in clan.members)
                packet.writeString(member.name);

            packet.writeInt16(clan.requests.len())
            foreach(request in clan.requests) {
                packet.writeInt16(request);
                packet.writeString(getPlayerName(request));
            }
            packet.send(pid, RELIABLE_ORDERED);
        break;
        case ClanPackets.AddMember:
            local clanId = packet.readInt16();
            
            if(!(clanId in Clan.getAll()))
                return;

            local clan = Clan.get(clanId);

            if(!clan.isLeader(pid))
                return;
            
            local request = packet.readString();
            request = split(request, "-");
            local requestId = request[0].tointeger();
            local requestName = request[1];

            clan.tryToAddRequestedUser(requestId, requestName);
        break;
        case ClanPackets.RemoveMember:
            local clanId = packet.readInt16();
            
            if(!(clanId in Clan.getAll()))
                return;

            local clan = Clan.get(clanId);
            local request = packet.readString();

            if(!clan.isLeader(pid) && getPlayerName(pid) != request)
                return;
            
            clan.tryToRemoveMember(request);
        break;
        case ClanPackets.JoinClan:
            local clanId = packet.readInt16();
            
            if(!(clanId in Clan.getAll()))
                return;

            local clan = Clan.get(clanId);

            local requestedUser = packet.readInt16();
            if(getPlayerClan(requestedUser) != -1)
                return;

            clan.addRequestForJoining(requestedUser);
        break;
    }
})