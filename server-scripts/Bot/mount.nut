
class createMount extends Bot
{
    mountedUserId = -1;

    constructor(name, x, y, z, angle, mountedUserId, armor, melee, ranged, headTxt, headModel, bodyTxt, bodyModel)
    {
        base.constructor(name);

        this.position = {x = x, y = y, z = z}
        this.angle = angle

        this.schemeId = BotScheme.Mount;
        this.mountedUserId = mountedUserId;

        this.armor = armor;
        this.melee = melee;
        this.ranged = ranged;
        
        this.headTxt = headTxt;
        this.headModel = headModel;
        this.bodyTxt = bodyTxt;
        this.bodyModel = bodyModel;

        this.onPositionUpdate();

        registerForClients();
    }

    function getAttackedByPlayer(pid)
    {
        stopMounting(mountedUserId);
    }

    function writeAdditionalInformations(packet) {
        packet.writeInt16(mountedUserId)   
        packet.writeInt16(armor)   
        packet.writeInt16(melee)   
        packet.writeInt16(ranged)   
        packet.writeInt16(headTxt)   
        packet.writeString(headModel)   
        packet.writeInt16(bodyTxt)   
        packet.writeString(bodyModel)   
    }
}