
addPacketListener(Packets.Bots, function(pid, packet)
{
    switch(packet.readUInt8())
    {
        case BotPackets.Synchronization:
            local length = packet.readInt16();

            for(local i = 0; i < length; i ++)
                onPlayerUpdateBot(pid, packet.readInt16(), packet.readFloat(), packet.readFloat(), packet.readFloat(), packet.readInt16());
        break;
        case BotPackets.Animation:
            setBotAnimation(packet.readInt16(), packet.readString());
        break;
        case BotPackets.Attack:
            local mode = packet.readUInt8();
            local botId = packet.readInt16();

            local bot = getBot(botId);
            if(bot == null)
                return;

            switch(mode)
            {
                case 1:
                    bot.attackPlayer(pid);
                break;
                case 2:
                    bot.getAttackedByPlayer(pid);
                break;
            }
        break;
    }
})