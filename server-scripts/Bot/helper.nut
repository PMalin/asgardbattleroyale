
local ticks = {};
local playersRelation = {};

class createHelper extends Bot
{
    helperUserId = -1;
    lastAttackerId = -1;
    lastAttackerIdType = -1;

    constructor(name, x, y, z, angle, helperUserId, armor, melee, ranged, headTxt, headModel, bodyTxt, bodyModel)
    {
        base.constructor(name);

        this.position = {x = x, y = y, z = z}
        this.angle = angle

        this.schemeId = BotScheme.Helper;
        this.helperUserId = helperUserId;

        this.armor = armor;
        this.melee = Items.id("ITMW_1H_SLD_AXE");
        this.ranged = -1;
        
        this.headTxt = headTxt;
        this.headModel = headModel;
        this.bodyTxt = bodyTxt;
        this.bodyModel = bodyModel;

        this.ai = BotAI.Search;

        this.enemy = -1;
        this.timer = 0;
        this.enemies = [];

        damage = 50,
        magicDamage = 50;
        magicProtection = 30;
        protection = 30;

        ticks[id] <- getTickCount() + 1000;
        playersRelation[id] <- helperUserId;

        this.onPositionUpdate();

        registerForClients();
    }

    function changeWeaponMode(value)
    {
        weaponMode = value;

        openWeapon(id, value);
    }

    function writeAdditionalInformations(packet) {
        packet.writeInt16(helperUserId)   
        packet.writeInt16(armor)   
        packet.writeInt16(melee)   
        packet.writeInt16(ranged)   
        packet.writeInt16(headTxt)   
        packet.writeString(headModel)   
        packet.writeInt16(bodyTxt)   
        packet.writeString(bodyModel)   
    }

    function onTimer() {
        if(ai == BotAI.Search)
            searchNearbyEnemy();
        else if(ai == BotAI.Run)
            runForEnemy();
        else if(ai == BotAI.Attack)
            attackEnemy();
        else if(ai == BotAI.Dead)
            goesDead();
    }

    function addEnemy(pid)
    {
        if (enemies.find(pid))
            return pid;

        if(isPlayerConnected(pid)) {
            foreach(bot in getPlayerHelpers(pid))
                bot.addEnemy(bot.id);
        }

        enemies.append(pid);
        return enemies[enemies.len()-1];
    }
 
    function removeEnemy(pid)
    {
        foreach(index, enemyId in enemies)
            if(enemyId == pid)
                enemies.remove(index);
    }
       
    function runForEnemy() {
        if(getEnemy() == -1)
        {
            ai = BotAI.Search;
            enemy = -1;
            setBotAnimation(id, "STOP");
            ticks[id] = getTickCount() + 1000;
            return;
        } 

        local posHelper = getPlayerPosition(helperUserId);
        if(getDistanceBetweenPositions(posHelper, posHelper) > 1000)
        {
            onPlayerUpdateBot(helperUserId, id, posHelper.x+30, posHelper.y, posHelper.z, 0);
            return;
        }

        local pObj = getPlayerPosition(helperUserId);

        if(getDistanceBetweenPositions(pObj, position) > 1000)
        {
            setBotPosition(id, pObj.x, pObj.y, pObj.z);
            ai = BotAI.Search;
            enemy = -1;
            setBotAnimation(id, "STOP");
            ticks[id] = getTickCount() + 1000;
            return;
        }             

        turnIntoEnemy();

        local distance = getDistanceBetweenPositions(getEnemyPosition(), position);

        if(distance <= 320)
            attackEnemy();
        else
            if(animation != "S_FISTRUNL") setBotAnimation(id, "S_FISTRUNL");

        ticks[id] = getTickCount() + 300;
    }

    function attackEnemy() {
        if(getEnemy() == -1)
        {
            ai = BotAI.Search;
            enemy = -1;
            setBotAnimation(id, "STOP");
            ticks[id] = getTickCount() + 100;
            return;
        } 

        local distance = getDistanceBetweenPositions(getEnemyPosition(), position);
        if(distance > 320)
        {
            ai = BotAI.Run;
            ticks[id] = getTickCount() + 100;    
            return;        
        }

        if(animation == "S_1HRUNL") {
            setBotAnimation(id, "T_1HATTACKMOVE");
            hitTarget();
            ticks[id] = getTickCount() + 500;
            lastAttackType = 1;
            return;
        }

        if(lastAttackType == 1)
            lastAttackType = 2;
        else
            lastAttackType = 1;

        timer ++;

        if(timer < 5) {
            ticks[id] = getTickCount() + 500;
            return;
        }

        timer = 0;
        local chance = rand() % 10;

        if(chance > 3) {
            hitTarget(lastAttackType);
        }else
            setBotAnimation(id, "T_FISTPARADEJUMPB");        
    }

    function searchNearbyEnemy() {
        ticks[id] = getTickCount() + 1000;

        local lastDistance = 1000;
        foreach(index, enemyId in enemies)
        {
            local positionObject = null;
            if(isNpc(enemyId))
            {
                local bObj = getBot(enemyId);
                if(bObj == null)
                {
                    enemies.remove(index);
                    continue;
                }
                if(bObj.health <= 0)
                {
                    enemies.remove(index);
                    continue;
                }
                positionObject = bObj.position;  
                local dist = getDistanceBetweenPositions(positionObject, position);
                if(dist < lastDistance)
                {
                    lastDistance = dist;
                    setEnemy(enemyId);
                }
                else if(dist > 1000)
                {
                    enemies.remove(index);
                    continue;
                }               
            }
            else 
            {
                if(!isPlayerConnected(enemyId))
                {
                    enemies.remove(index);
                    continue;
                }
                if(!isPlayerSpawned(enemyId))
                {
                    enemies.remove(index);
                    continue;
                }
                positionObject = getPlayerPosition(enemyId);    
                local dist = getDistanceBetweenPositions(positionObject, position);
                if(dist < lastDistance)
                {
                    lastDistance = dist;
                    setEnemy(enemyId);
                }
                else if(dist > 1000)
                {
                    enemies.remove(index);
                    continue;
                }  
            }
        }

        if(enemy == -1)
        {
            local posHelper = getPlayerPosition(helperUserId);
            local _angle = getVectorAngle(position.x,position.z,posHelper.x,posHelper.z);
            local angleDiff = abs(_angle - angle);

            if(angleDiff > 10)
                setBotAngle(id, _angle);

            local dist = getDistanceBetweenPositions(posHelper, position);
            if(dist < 200)
                setBotAnimation(id, "STOP");
            else if(dist > 1500)
            {
                onPlayerUpdateBot(helperUserId, id, posHelper.x+30, posHelper.y, posHelper.z, 0);
            }
            else if(dist > 400) {
                if(weaponMode == true)
                    setBotAnimation(id, "S_1HRUNL");
                else
                    setBotAnimation(id, "S_RUNL");
            }else {
                if(weaponMode == true)
                    setBotAnimation(id, "S_1HWALKL");
                else
                    setBotAnimation(id, "S_WALKL");
            }
        }
    }

    function setEnemy(enemyId) {
        enemy = enemyId;
        ai = BotAI.Run;

        openWeapon(id, true);
        setBotAnimation(id, "S_1HRUNL");
    }

    function getEnemy() {
        if(enemy == -1)
            return enemy;

        if(isPlayer(enemy))
        {
            if(!isPlayerConnected(enemy))
                return -1;
            if(!isPlayerSpawned(enemy))
                enemy = -1;
        }else {
            local bObj = getBot(enemy);
            if(bObj == null)
                return -1;
            if(bObj.health <= 0)
                enemy = -1;
        }
        
        return enemy;
    }

    function getEnemyPosition()
    {
        if(isPlayer(enemy))
            return getPlayerPosition(enemy);
        else
            return getBotPosition(enemy);
    }

    function turnIntoEnemy()
    {
        local pos = getEnemyPosition();
        local _angle = getVectorAngle(position.x,position.z,pos.x,pos.z);
        local angleDiff = abs(_angle - angle);

        if(angleDiff > 10)
            setBotAngle(id, _angle);
    }

    function hitTarget(lastAttackType = 0) {
        local packet = Packet(Packets.Bots);
        packet.writeUInt8(BotPackets.Attack);
        packet.writeInt16(id);
        packet.writeInt16(enemy);
        packet.writeInt16(lastAttackType);
        packet.sendToPlayersInTable(RELIABLE_ORDERED, visiblePlayers);     

        if(isNpc(enemy))
            getBot(enemy).getAttackedByNpc(this);   
    }

    function attackPlayer(pid) {
        local dmg = calculateDamageToPlayerByNPC(this, pid, DAMAGE_BEAST);

        if(dmg <= 3)
            dmg = 3;

        local hp = getPlayerHealth(pid) - dmg;
        if(hp < 0) {
            hp = 0;
            setBotAnimation(id, "STOP");
            enemy = -1;
            ai = BotAI.Search;
        }

        setPlayerHealth(pid, hp);
    }

    function playerKillBot(pid) {
        ticks[id] = getTickCount() + 5000;
        ai = BotAI.Dead;
    }

    function getAttackedByPlayer(pid) {
        local dmg = calculateDamageForNPC(pid, this);

        if(getDistanceBetweenPositions(getPlayerPosition(pid), position) >= 1200)
            return;

        if(getEnemy() == -1)
        {
            local newEnemyId = addEnemy(pid);
            setEnemy(newEnemyId);
            ai = BotAI.Run;
            ticks[id] = getTickCount() + 100;
        }

        if(lastAttackerId == pid && getEnemy() != pid)
        {
            local newEnemyId = addEnemy(pid);
            setEnemy(newEnemyId);
        }

        lastAttackerId = pid;

        if(dmg <= 3)
            dmg = 3;

        health = health - dmg;

        if(health <= 0) {
            health = 0;
            playerKillBot(pid);
            removeBot(id);
            return;
        }
        
        setBotHealth(id, health);
    }
    
    function getAttackedByNpc(bot)
    {
        local dmg = calculateDamageToNPCByNPC(bot, this, DAMAGE_BEAST);

        if(getEnemy() == -1)
        {
            local newEnemyId = addEnemy(bot.id);
            setEnemy(newEnemyId);
            ai = BotAI.Run;
            ticks[id] = getTickCount() + 100;
        }

        if(lastAttackerId == bot.id && getEnemy() != bot.id)
        {
            local newEnemyId = addEnemy(bot.id);
            setEnemy(newEnemyId);
        }

        lastAttackerId = bot.id;

        if(dmg <= 3)
            dmg = 3;

        health = health - dmg;

        if(health <= 0) {
            health = 0;
            removeBot(id);
            return;
        }
        
        setBotHealth(id, health);    
    }

    function goesDead()
    {
        removeBot(id);
    }

    function beforeRemove()
    {
        playersRelation.rawdelete(id);
        ticks.rawdelete(id);
    }

    ai = -1;
    enemy = -1;
    enemies = [];

    timer = -1;
}

function Bot_addHelperBotsEnemy(pid, enemyId)
{
    foreach(bot_id, relation_player in playersRelation)
    {
        if(relation_player != pid)
            continue;

        getBot(bot_id).addEnemy(enemyId);
    }
}

setTimer(function () {
    local current = getTickCount();
    foreach(botId, time in ticks)
    {
        if(time <= current) {
            getBot(botId).onTimer();
        }
    }
}, 100, 0);

function clearBotTicks() {
    clearMonsterBotTicks();
    clearBossBotTicks();
    ticks.clear();
}

function Bot_openHelpersWeapons(player, value) {
    foreach(id, pid in playersRelation)
        if(player == pid)
            getBot(id).changeWeaponMode(value);
}

function Bot_removeHelpers(player) {
    foreach(id, pid in playersRelation)
        if(player == pid)
            getBot(id).changeWeaponMode(value);
}
