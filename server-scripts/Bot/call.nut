
function addMountForPlayer(pid) {
    if(getPlayer(pid).mounting == true)
    {
        stopMounting(pid);
        return;
    }

    local pos = getPlayerPosition(pid);
    local armor = getPlayerArmor(pid);
    local melee = getPlayerMeleeWeapon(pid);
    local ranged = getPlayerRangedWeapon(pid);

    local visual = getPlayerVisual(pid);

    createMount("Jezdziec "+getPlayerName(pid),pos.x, pos.y, pos.z, getPlayerAngle(pid), pid, armor, melee, ranged, visual.headTxt, visual.headModel, visual.bodyTxt, visual.bodyModel);

    setPlayerInstance(pid, "SHADOWBEAST");

    local obj = getPlayer(pid);

    setPlayerStrength(pid, obj.str);
    setPlayerDexterity(pid, obj.dex);

    setPlayerHealth(pid, obj.hp);
    setPlayerMaxHealth(pid, obj.maxHp);
    setPlayerMana(pid, obj.mana);
    setPlayerMaxMana(pid, obj.maxMana);

    getPlayer(pid).mounting = true;
}

function stopMounting(pid) {
    if(getPlayer(pid).mounting == false)
        return;

    getPlayer(pid).mounting = false;

    local pos = getPlayerPosition(pid);
    local armor = getPlayerArmor(pid);
    local melee = getPlayerMeleeWeapon(pid);
    local ranged = getPlayerRangedWeapon(pid);

    local visual = getPlayerVisual(pid);

    setPlayerInstance(pid, "PC_HERO");
    destroyMount(pid);

    local obj = getPlayer(pid);

    setPlayerStrength(pid, obj.str);
    setPlayerDexterity(pid, obj.dex);

    setPlayerHealth(pid, obj.hp);
    setPlayerMaxHealth(pid, obj.maxHp);
    setPlayerMana(pid, 100000);
    setPlayerMaxMana(pid, 100000);

    setPlayerMagicLevel(pid, 6);

    setPlayerSkillWeapon(pid, 0, 100);
    setPlayerSkillWeapon(pid, 1, 100);
    setPlayerSkillWeapon(pid, 2, 500);
    setPlayerSkillWeapon(pid, 3, 500);

    getItems(pid).respawn();

    setPlayerVisual(pid, obj.bodyModel, obj.bodyTxt, obj.headModel, obj.headTxt);
}