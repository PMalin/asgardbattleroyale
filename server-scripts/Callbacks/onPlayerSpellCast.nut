
addEventHandler("onPlayerSpellCast", function(pid, id)
{
	local instance = Items.name(id);
    if(hasPlayerItem(pid, instance) < 1)
    {
        kick(pid, "Wyst�pi� b��d!");
        return;
    }

    setPlayerMagicWeapon(pid, id);

	if(instance.find("ITSC_") == null){
		return;
	}

    removeItem(pid, instance, 1, false);
});
