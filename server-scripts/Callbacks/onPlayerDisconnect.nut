
addEventHandler("onPlayerDisconnect", function (pid, res) {
    getPlayer(pid).clear();
    getItems(pid).clear();

    Bot_onPlayerDisconnect(pid, res);
    Clan.onDisconnect(pid);

    checkOnDisconnectOrcPlayer(pid);
})

