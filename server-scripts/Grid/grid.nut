
grid <- {};

grid._bucket <- {};

function grid::index(x, z) {
    return (x / 1000).tointeger() + "," + (z / 1000).tointeger()
}

function grid::recognize(index) {
    if(!(index in grid._bucket))
        return grid._bucket[index] <- { bots = [], players = [], vobs = [] }

    return grid._bucket[index];
}

function grid::getChunks(index, _radius = 2) {
    local pos = split(index, ",")
    local chunk = {
        x = pos[0].tointeger(),
        z = pos[1].tointeger(),
    }

    local chunks = [];

    for (local x = chunk.x - _radius, endX = chunk.x + _radius; x <= endX; ++x)
    {
        for (local z = chunk.z - _radius, endZ = chunk.z + _radius; z <= endZ; ++z)
        {
            chunks.append(x + "," + z);
        }
    }

    return chunks;
}

function grid::removePlayer(index, pid) {
    if(!(index in grid._bucket))
        return;

    local bucketObj = grid._bucket[index];

    foreach(bot in bucketObj.bots)
        getBot(bot).unspawn(pid);

    bucketObj.players.remove(bucketObj.players.find(pid));
}

function grid::addPlayer(index, pid) {
    if(!(index in grid._bucket))
        return;

    local bucketObj = grid._bucket[index];

    foreach(bot in bucketObj.bots)
        getBot(bot).spawn(pid);

    bucketObj.players.append(pid);
}

function grid::removeBot(index, botid) {
    if(!(index in grid._bucket))
        return;

    local bucketObj = grid._bucket[index];
    bucketObj.bots.remove(bucketObj.bots.find(botid));
}

function grid::addBot(index, botid) {
    if(!(index in grid._bucket))
        return;

    local bucketObj = grid._bucket[index];
    bucketObj.bots.append(botid);
}

function grid::clearGridFromBots()
{
    foreach(index in grid._bucket)
        index.bots.clear();
}

function grid::onPlayerPositionChange(pid, x, y, z) {
    local pObj = getPlayer(pid);
    local currChunk = grid.index(x,z);

    if(currChunk != pObj.gridIndex) {
        local oldChunks = clone pObj.chunks;
        pObj.gridIndex = currChunk;

        pObj.chunks = grid.getChunks(currChunk);
        foreach(chunk in pObj.chunks)
        {
            local isInOld = oldChunks.find(chunk);

            if(isInOld == null)
            {
                grid.recognize(chunk);
                grid.addPlayer(chunk, pid);
            }else{
                oldChunks.remove(isInOld);
            }
        }

        foreach(chunk in oldChunks)
            grid.removePlayer(chunk, pid);
    }
}

addEventHandler("onPlayerPositionChange", grid.onPlayerPositionChange);

function grid::onBotPositionChange(botid, x, y, z) {
    local bObj = getBot(botid);
    local currChunk = grid.index(x,z);

    if(currChunk != bObj.gridIndex) {
        grid.removeBot(bObj.gridIndex, botid);
        bObj.gridIndex = currChunk;
        grid.recognize(currChunk);
        grid.addBot(bObj.gridIndex, botid);

        local nearPlayers = grid.getChunks(currChunk, 1);
        foreach(chunkPlayer in nearPlayers)
            foreach(playerId in grid.recognize(chunkPlayer).players)
                bObj.spawn(playerId);
    }
}