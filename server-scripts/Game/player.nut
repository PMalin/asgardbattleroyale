
Game.acceptGame <- 0;
Game.totalInGame <- 0;
Game.timeoutOnePlayer <- 0;

function Game::checkTotalInGamePlayers()
{
    Game.totalInGame = 0;
    foreach(player in getPlayers())
        if(player.inGame && player.isObserver == false)
            Game.totalInGame++;
}

function Game::checkTotalReadyPlayers()
{
    local readyToPlayPlayers = 0;
    foreach(player in getPlayers())
        if(player.readyToPlay)
            readyToPlayPlayers++;

    return readyToPlayPlayers;
}

function Game::checkOnlinePlayersInLobby()
{
    local onlinePlayers = 0;
    foreach(pid, player in getAllPlayers())
        if(player != null)
            onlinePlayers++;

    return onlinePlayers;
}

function Game::checkPlayerStateInGame()
{
    Game.checkTotalInGamePlayers();

    if(Game.timeoutOnePlayer != 0)
    {
        if(Game.totalInGame == 1)
        {
            Game.timeoutOnePlayer = Game.timeoutOnePlayer - 1;

            if(Game.timeoutOnePlayer <= 1)
            {
                local message = "";
                Game.timeoutOnePlayer = 0;
                foreach(player in getPlayers())
                {
                    if(player.inGame && player.isObserver == false)
                    {
                        player.winGames = player.winGames + 1;
                        message = " Zwyci�y�: "+getPlayerName(player.id);
                        sendAdditionalInformations(player.id);
                        break;
                    }
                }

                Game.Controller.endGame();
                addLobbyQuerie("Gra zosta�a zako�czona."+message);
            }
            return;
        }else{
             Game.timeoutOnePlayer = 0;
        }
    }

    if(Game.totalInGame == 0)
        Game.Controller.endGame();
    else if(Game.totalInGame == 1) {
        if(Game.timer < 240)
            Game.timeoutOnePlayer = 2;
    else {
        Game.timeoutOnePlayer = 30;
        sendMessageToAll(255, 255, 255, "Zosta� 1 gracz w grze. Zosta�o 30 sekund odliczania do wygranej.")
    }
    }
}

function Game::checkGameCanBeStarted()
{
    local players = Game.checkTotalReadyPlayers();
    local allplayers = Game.checkOnlinePlayersInLobby();
        Game.timer = 10;
        Game.lobbyChatBlocked = true;

    local packet = Packet(Packets.Game);
    packet.writeUInt8(GamePackets.ResetState);
    packet.sendToAll(RELIABLE_ORDERED);

    if(allplayers < 4)
        return;

    if(players > abs(allplayers/2))
    {
        Game.timer = 10;
        Game.lobbyChatBlocked = true;

        local packet = Packet(Packets.Game);
        packet.writeUInt8(GamePackets.ResetState);
        packet.sendToAll(RELIABLE_ORDERED);
    }
}

function Game::goOnRandomSpawn(pid)
{
    local randPos = [
        [-4498.13, -1005.94, -6980.94],
        [-12852.6, -578.125, -5371.95],
        [-14266.1, -277.266, 1125.86],
        [-19916.3, -328.047, -1023.91],
        [-27733.4, -4433.05, 5094.22],
        [-31608.9, -195.156, 8503.52],
        [-32767.8, 942.578, 9036.17],
        [-35701.4, -1851.17, 7757.81],
        [-18776.3, -2895.94, 20503],
        [-9148.28, -3785.16, 25881.3],
        [17585.4, -4962.42, 9571.02],
        [28640.5, -3953.05, 12381.4],
        [33445.7, -5167.42, -3243.59],
        [29054.8, -4448.13, -11605]
    ];

    switch(activeWorld)
    {
        case Worlds.Khorinis:
            randPos = [
                [8600,1297,-18236,338.2],
                [7902,364,-5213,0.111058],
                [9605,368,4908,12.3172],
                [31434,3663,7095,168.367],
                [30633,4257,-734,191.775],
                [40210,3818,-1839,85.0718],
                [44338,3556,7322,38.1478],
                [55742,3153,11892,80.5092],
                [61337,4415,9043,115.462],
                [64486,4764,11323,13.0017],
                [72515,4858,18973,339.317],
            ]
        break;
        case Worlds.Dolina:
            randPos = [
                [2176,247,-649,98.5592],
                [9723,-397,222,4.82238],
                [-962,-1120,7233,261.569],
                [-10290,-1091,6959,294.094],
                [-6912,-851,14664,45.9489],
                [4341,6002,28167,97.4441],
                [9357,-1296,14758,86.262],
                [14910,99,5490,156.407],
                [7521,-59,-9447,172.773],
                [4867,5134,-17258,190.157],
                [-14179,1408,-23577,22.6067],
                [-17170,-1317,-9631,220.717],
                [-15088,-1220,3731,28.5978],
                [-38777,925,4889,220.051],
            ]
        break;
    }

    local randObj = randPos[rand() % randPos.len()];
    setPlayerPosition(pid, randObj[0], randObj[1], randObj[2])
}