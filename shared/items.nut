RegisteredItems <- {
    "ITAR_BRHELMETADDON_01" : {
        name = "Lachy na dupsko",

        flag = ITEM_KAT_ARMOR,
        description = "Pierwszy tw�j armorek frajerze.",

        protection = 10,
        magicProtection = 10,

        value = 30,
    },
    "ITAR_BRHELMETADDON_07" : {
        name = "Lachy na dupsko",

        flag = ITEM_KAT_ARMOR,
        description = "Pierwszy tw�j armorek frajerze.",

        protection = 10,
        magicProtection = 10,

        value = 30,
    },
    "ITAR_BRHELMETADDON_02" : {
        name = "Lachy na dupsko",

        flag = ITEM_KAT_ARMOR,
        description = "Pierwszy tw�j armorek frajerze.",

        protection = 10,
        magicProtection = 10,

        value = 30,
    },
    "ITAR_BRHELMETADDON_03" : {
        name = "Lachy na dupsko",

        flag = ITEM_KAT_ARMOR,
        description = "Pierwszy tw�j armorek frajerze.",

        protection = 10,
        magicProtection = 10,

        value = 30,
    },
    "ITAR_BRHELMETADDON_04" : {
        name = "Lachy na dupsko",

        flag = ITEM_KAT_ARMOR,
        description = "Pierwszy tw�j armorek frajerze.",

        protection = 10,
        magicProtection = 10,

        value = 30,
    },
    "ITAR_BRHELMETADDON_05" : {
        name = "Lachy na dupsko",

        flag = ITEM_KAT_ARMOR,
        description = "Pierwszy tw�j armorek frajerze.",

        protection = 10,
        magicProtection = 10,

        value = 30,
    },
    "ITAR_BRHELMETADDON_06" : {
        name = "Lachy na dupsko",

        flag = ITEM_KAT_ARMOR,
        description = "Pierwszy tw�j armorek frajerze.",

        protection = 10,
        magicProtection = 10,

        value = 30,
    },
    "ITAR_BRADDON_01" : {
        name = "Lachy na dupsko",

        flag = ITEM_KAT_ARMOR,
        description = "Pierwszy tw�j armorek frajerze.",

        protection = 10,
        magicProtection = 10,

        value = 30,
    },
    "ITAR_BRADDON_02" : {
        name = "Lachy na dupsko",

        flag = ITEM_KAT_ARMOR,
        description = "Pierwszy tw�j armorek frajerze.",

        protection = 10,
        magicProtection = 10,

        value = 30,
    },
    "ITAR_BRADDON_03" : {
        name = "Lachy na dupsko",

        flag = ITEM_KAT_ARMOR,
        description = "Pierwszy tw�j armorek frajerze.",

        protection = 10,
        magicProtection = 10,

        value = 30,
    },
    "ITAR_BRADDON_04" : {
        name = "Lachy na dupsko",

        flag = ITEM_KAT_ARMOR,
        description = "Pierwszy tw�j armorek frajerze.",

        protection = 10,
        magicProtection = 10,

        value = 30,
    },
    "ITAR_BRADDON_05" : {
        name = "Lachy na dupsko",

        flag = ITEM_KAT_ARMOR,
        description = "Pierwszy tw�j armorek frajerze.",

        protection = 10,
        magicProtection = 10,

        value = 30,
    },
    "ITAR_PRISONER" : {
        name = "Lachy na dupsko",

        flag = ITEM_KAT_ARMOR,
        description = "Pierwszy tw�j armorek frajerze.",

        protection = 10,
        magicProtection = 10,

        value = 30,
    },
    "ITAR_RANGER_ADDON" : {
        name = "Zbroja wodnego kr�gu",

        flag = ITEM_KAT_ARMOR,
        description = "Pierwszy tw�j armorek frajerze.",

        protection = 15,
        magicProtection = 15,

        value = 50,
    },
    "ITAR_BLOODWYN_ADDON" : {
        name = "�rednia zbrojka",

        flag = ITEM_KAT_ARMOR,
        description = "Jest gyt.",

        protection = 20,
        magicProtection = 15,

        value = 50,
    },
    "ITAR_THORUS_ADDON" : {
        name = "Fajna zbroja ci�ka",

        flag = ITEM_KAT_ARMOR,
        description = "Dobry nawet ten armorek koliego..",

        protection = 30,
        magicProtection = 15,

        value = 50,
    },
    "ITAR_PIR_M_ADDON" : {
        name = "Zbroja pirata",

        flag = ITEM_KAT_ARMOR,
        description = "Silny pirat nosi takie wdzianko.",

        protection = 25,
        magicProtection = 15,

        value = 50,
    },
    "ITAR_SLD_M" : {
        name = "Zbroja najemnika",

        flag = ITEM_KAT_ARMOR,
        description = "Silny najemnik nosi takie wdzianko.",

        protection = 30,
        magicProtection = 5,

        value = 50,
    },
    "ITAR_OREBARON_ADDON" : {
        name = "Zbroja czytera",

        flag = ITEM_KAT_ARMOR,
        description = "WTF?!",

        protection = 59,
        magicProtection = 59,

        value = 50,
    },
    "ITAR_XARDAS" : {
        name = "Szata czarnego maga",

        flag = ITEM_KAT_ARMOR,
        description = "Szata xardiego",

        protection = 33,
        magicProtection = 66,

        value = 50,
    },
    "ITAR_CORANGAR" : {
        name = "Zbroja �wi�tynna",

        flag = ITEM_KAT_ARMOR,
        description = "�wi�tynna zbroja dla �wi�tynnego wojownika kurwa.",

        protection = 40,
        magicProtection = 20,

        value = 50,
    },
    "ITAR_RAVEN_ADDON" : {
        name = "Zbroja kruka",

        flag = ITEM_KAT_ARMOR,
        description = "Totalna odporno�� na obra�enia.",

        protection = 50,
        magicProtection = 30,

        value = 50,
    },
    "ITAR_KDW_L_Addon" : {
        name = "Szata maga",

        flag = ITEM_KAT_ARMOR,
        description = "A pod kiec� p� literka.",

        protection = 10,
        magicProtection = 35,

        value = 50,
    },
    "ITAR_KDW_H" : {
        name = "Ci�ka szata maga",

        flag = ITEM_KAT_ARMOR,
        description = "Zbroja dla prawdziwego maga.",

        protection = 15,
        magicProtection = 45,

        value = 50,
    },
    "ITAR_KDF_H" : {
        name = "Szata arcymaga",

        flag = ITEM_KAT_ARMOR,
        description = "Szata co nie dotknie cie �oden magiczny pocisk.",

        protection = 20,
        magicProtection = 60,

        value = 50,
    },
    "ITMW_SHORTSWORD4": {
        name = "Mieczyk z dupska",

        flag = ITEM_KAT_MELEE,
        damage = 100,

        description = "Kr�tki no�yk.",

        value = 122,
    },
    "ITMW_BRADDON_01": {
        name = "Mieczyk z dupska",

        flag = ITEM_KAT_MELEE,
        damage = 100,

        description = "Kr�tki no�yk.",

        value = 122,
    },
    "ITMW_BRADDON_02": {
        name = "Mieczyk z dupska",

        flag = ITEM_KAT_MELEE,
        damage = 100,

        description = "Kr�tki no�yk.",

        value = 122,
    },
    "ITMW_BRADDON_03": {
        name = "Mieczyk z dupska",

        flag = ITEM_KAT_MELEE,
        damage = 100,

        description = "Kr�tki no�yk.",

        value = 122,
    },
    "ITMW_BRADDON_04": {
        name = "Mieczyk z dupska",

        flag = ITEM_KAT_MELEE,
        damage = 100,

        description = "Kr�tki no�yk.",

        value = 122,
    },
    "ITMW_BRADDON_05": {
        name = "Mieczyk z dupska",

        flag = ITEM_KAT_MELEE,
        damage = 100,

        description = "Kr�tki no�yk.",

        value = 122,
    },
    "ITMW_BRADDON_06": {
        name = "Mieczyk z dupska",

        flag = ITEM_KAT_MELEE,
        damage = 100,

        description = "Kr�tki no�yk.",

        value = 122,
    },
    "ITMW_BRADDON_07": {
        name = "Mieczyk z dupska",

        flag = ITEM_KAT_MELEE,
        damage = 100,

        description = "Kr�tki no�yk.",

        value = 122,
    },
    "ITMW_1H_SLD_SWORD": {
        name = "Mieczor najemnika",

        flag = ITEM_KAT_MELEE,
        damage = 130,

        description = "Miecz najemnika taki zardzewia�y.",

        value = 122,
    },
    "ITMW_SCHWERT3": {
        name = "Miecz p�tor�czny",

        flag = ITEM_KAT_MELEE,
        damage = 160,

        description = "Miecz p�tor�czny ma zajebisty zasi�g.",

        value = 122,
    },
    "ITMW_1H_BLESSED_01": {
        name = "Mieczor paladyna",

        flag = ITEM_KAT_MELEE,
        damage = 160,

        description = "B�ogos�awiony miecz w ciul dobry.",

        value = 122,
    },
    "ITMW_1H_SPECIAL_04": {
        name = "Mieczor �owcy smok�w",

        flag = ITEM_KAT_MELEE,
        damage = 200,

        description = "Zasi�g zajebisty i damage, �e japierdole.",

        value = 122,
    },
    "ITMW_2H_SLD_SWORD": {
        name = "2H Swordzik najemnika",

        flag = ITEM_KAT_MELEE,
        damage = 140,

        description = "Zardzewia�e g�wno.",

        value = 122,
    },
    "ITMW_2H_SLD_AXE": {
        name = "Topor najemnika",

        flag = ITEM_KAT_MELEE,
        damage = 170,

        description = "Top�r dobry ale zasieg kr�ciutki..",

        value = 122,
    },
    "ITMW_ZWEIHAENDER4": {
        name = "Miecz 2h zajebisty",

        flag = ITEM_KAT_MELEE,
        damage = 200,

        description = "Pasuje idealnie do zbrojki cor angara.",

        value = 122,
    },
    "ITMW_BERSERKERAXT": {
        name = "Top�r berserkera",

        flag = ITEM_KAT_MELEE,
        damage = 300,

        description = "Najlepsza bro� w grze chyba.",

        value = 122,
    },
    "ITMW_2H_PAL_SWORD": {
        name = "Mieczor paladyna",

        flag = ITEM_KAT_MELEE,
        damage = 240,

        description = "Palady�ski miecz tylko lepszy ni� w grze.",

        value = 122,
    },
    "ITMW_2H_SPECIAL_04": {
        name = "Mieczor �owcy smok�w",

        flag = ITEM_KAT_MELEE,
        damage = 240,

        description = "Miecz 2h ten co dla �owcy smok�w by�.",

        value = 122,
    },
    "ITMW_BELIARWEAPON_1H_01": {
        name = "Czyterskie narz�dzie",

        flag = ITEM_KAT_MELEE,
        damage = 240,

        description = "Miecz bo�y.",

        value = 122,
    },
    "ITRW_BOLT": {
        name = "Be�t",

        flag = ITEM_KAT_OTHER,
        description = "Pozwala strzela� z kuszy",

        value = 1,
    },
    "ITRW_ARROW": {
        name = "Strza�a",

        flag = ITEM_KAT_OTHER,
        description = "Pozwala strzela� z �uku",

        value = 1,
    },
    "ITRU_ZAP": {
        name = "Piorun",

        flag = ITEM_KAT_RUNE,
        magicDamage = 80,

        description = "Ma�y grubasek.",

        value = 50,
    },
    "ITRU_INSTANTFIREBALL": {
        name = "Kula ognia",

        flag = ITEM_KAT_RUNE,
        magicDamage = 90,

        description = "Najlepsza bro� do rozpalenia w piecu.",

        value = 50,
    },
    "ITRU_CONCUSSIONBOLT": {
        name = "Kula zniszczenia",

        flag = ITEM_KAT_RUNE,
        magicDamage = 110,

        description = "Dobra kula do napierdalania.",

        value = 50,
    },
    "ITRU_BELIARSRAGE": {
        name = "Gniew Beliara",

        flag = ITEM_KAT_RUNE,
        magicDamage = 200,

        description = "I po tobie.",

        value = 30000,
    },
    "ITRU_WATERFIST": {
        name = "Pi�� wody",

        flag = ITEM_KAT_RUNE,
        magicDamage = 170,

        description = "Pi�� z wody.",

        value = 30000,
    },
    "ITRU_WINDFIST": {
        name = "Pi�� wiatru",

        flag = ITEM_KAT_RUNE,
        magicDamage = 140,

        description = "Pi�� z wichruu.",

        value = 30000,
    },
    "ITRU_THUNDERBALL": {
        name = "B�yskawica",

        flag = ITEM_KAT_RUNE,
        magicDamage = 170,

        description = "Ale nakurwia!.",

        value = 100000,
    },
    "ITRW_CROSSBOW_L_01": {
        name = "Lekka kusza",

        flag = ITEM_KAT_RANGED,
        damage = 130,

        description = "Jaka� tam kusza fiutku �mierdz�cy.",

        value = 5000,
    },
    "ITRW_CROSSBOW_M_01": {
        name = "Kusza �rednia",

        flag = ITEM_KAT_RANGED,
        damage = 150,

        description = "Jaka� tam kusza fiutku �mierdz�cy.",

        value = 5000,
    },
    "ITRW_CROSSBOW_H_01": {
        name = "Kusza ci�ka",

        flag = ITEM_KAT_RANGED,
        damage = 170,

        description = "Jaka� tam kusza fiutku �mierdz�cy.",

        value = 5000,
    },
    "ITRW_SLD_BOW": {
        name = "�uk najemnika",

        flag = ITEM_KAT_RANGED,
        damage = 110,

        description = "Dobry �uk i wygl�da zajebi�cie prosto.",

        value = 1000,
    },
    "ITRW_BOW_H_03": {
        name = "�uk Quara",

        flag = ITEM_KAT_RANGED,
        damage = 140,

        description = "Tak nazwa�em swoim nickiem, i chuj ci w dupe.",

        value = 100000,
    },
    "ITRW_BOW_M_02": {
        name = "�uk jesionowy",

        flag = ITEM_KAT_RANGED,
        damage = 130,

        description = "Jesionowy �uk.",

        value = 100000,
    },
    "ITRW_BOW_L_01": {
        name = "�uk Zyksa",

        flag = ITEM_KAT_RANGED,
        damage = 70,

        description = "Bezu�yteczny jak zyks.",

        value = 1,
    },
}
